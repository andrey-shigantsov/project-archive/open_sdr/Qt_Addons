#include <SDR/Qt/Sources/AudioInput.h>
#include <SDR/Qt_Addons/IO/TAudioInput.h>

using namespace SDR;

static inline TAudioInput * d(AudioInput_t *This)
{
  return static_cast<TAudioInput *>(This->d);
}

void init_AudioInput(AudioInput_t *This, AudioInputCfg_t *Cfg)
{
  QAudioFormat format;
  format.setSampleRate(Cfg->Fs);
  format.setSampleType(QAudioFormat::SignedInt);
  format.setSampleSize(16);
  format.setCodec("audio/pcm");
  TAudioInput * d = new TAudioInput(format);
  This->d = d;
}

void release_AudioInput(AudioInput_t *This)
{
  delete d(This);
}

void AudioInput_setCallback_bufready(AudioInput_t * This, AudioInputCallback_bufready_t * Cbk)
{
  d(This)->setSink(Cbk->Master, Cbk->fxn);
}

void AudioInput_setCallback_bufready_iq(AudioInput_t * This, AudioInputCallback_bufready_iq_t * Cbk)
{
  d(This)->setSink(Cbk->Master, Cbk->fxn);
}

void AudioInput_start(AudioInput_t * This)
{
  d(This)->start();
}
void AudioInput_stop(AudioInput_t * This)
{
  d(This)->stop();
}

void AudioInput_DevicesList(AudioInput_t *This)
{
//  QAudioDeviceInfo inputInfo = QAudioDeviceInfo::defaultInputDevice();
  QAudioDeviceInfo::availableDevices(QAudio::AudioInput);
}
