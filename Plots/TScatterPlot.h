#ifndef TSCATTERPLOT_H_
#define TSCATTERPLOT_H_

#include "TPlot.h"

namespace SDR
{

class TScatterPlot : public TPlot
{
  Q_OBJECT
public:
  explicit TScatterPlot(QWidget *parent = 0);
  ~TScatterPlot();

  void clearData();

  inline void setRefreshCount(int Count){RefreshCount = Count;}
  inline void setSymbolsCount(int Count){setRefreshCount(Count);}

  void append(iqSample_t &data);
  void append(iqSample_t * data, Size_t size);
  void append_and_refresh(iqSample_t *data, Size_t size);

protected:
  QCPCurve *valuesCurve;
  QVector<iqSample_t> Collector;
  QVector<double> *Keys, *Values;
  int RefreshCount;
};

} // SDR

#endif // TSCATTERPLOT_H_
