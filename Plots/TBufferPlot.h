#ifndef TBUFFERPLOT_H
#define TBUFFERPLOT_H

#include "TPlot.h"
#include <QVector>

namespace SDR
{

class TBufferPlot : public TPlot
{
  Q_OBJECT
public:
  enum EType {typeBuffer, typeBufferIQ, typeFreqTwoSided};
  explicit TBufferPlot(TBufferPlot::EType type = TBufferPlot::typeBuffer, QWidget * parent = 0);
  ~TBufferPlot();

  inline void setFreqType(Frequency_t fs)
  {
    Fs = fs? fs : 1;
    setType(typeFreqTwoSided);
  }
  void setType(EType type);
  void setColor(QColor color){PlotableRe->setPen(QPen(color));}

  void setAveragingEnable(bool enable);
  void setAveragingCount(Size_t count){AveragingCount = count;}

  void clearData();

  void append(Sample_t* data, Size_t size);
  void append(iqSample_t* data, Size_t size);
  void raw_append(Sample_t* data, Size_t size);
  void raw_append(iqSample_t* data, Size_t size);

  inline QCPCurve* getCurveRe(){return PlotableRe;}
  inline QCPCurve* getCurveIm(){return PlotableIm;}

  void setScatterStyle(QCPScatterStyle ss);
  void setLineStyle(QCPCurve::LineStyle ls);

public slots:
  bool setParam(QString & name, QString &value);

private:
  EType Type; Frequency_t Fs;
  QCPCurve *PlotableRe, *PlotableIm;
  QVector<double> *Keys, *ValuesRe, *ValuesIm;

  Size_t AveragingCount, AveragingCounter;
  bool flag_isAveraging, Averaging_flag_isFirst;

  QVector<Sample_t> averagingReBuf;
  QVector<iqSample_t> averagingReImBuf;
};

} // SDR

#endif // TBUFFERPLOT_H
