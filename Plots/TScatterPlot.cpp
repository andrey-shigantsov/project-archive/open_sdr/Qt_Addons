
#include "TScatterPlot.h"

using namespace SDR;

SDR::TScatterPlot::TScatterPlot(QWidget *parent) :
  TPlot("Scatter", parent)
{
  setAspectRatio(QSize(1,1));

  valuesCurve = createCurve(QPen(Qt::green));
  valuesCurve->setLineStyle(QCPCurve::lsNone);
  valuesCurve->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssStar, 4));

  Keys = new QVector<double>;
  Values = new QVector<double>;

  RefreshCount = 128;

  setCustomCollectDataEnabled(true);
  setCollectData(true);
}

TScatterPlot::~TScatterPlot()
{
  delete Keys;
  delete Values;
}

void SDR::TScatterPlot::clearData()
{
  Collector.clear();
  Keys->clear();
  Values->clear();
  valuesCurve->data().data()->clear();
}

void TScatterPlot::append(iqSample_t &data)
{
  if (isCollectData)
  {
    if (Collector.size() >= RefreshCount)
      Collector.removeFirst();
    Collector.append(data);
    append_and_refresh(Collector.data(), Collector.size());
  }
  else
  {
    Keys->append(data.i);
    Values->append(data.q);
    if (Values->size() == RefreshCount)
      send_data_to_refresh(valuesCurve, Keys, Values, true);
  }
}

void TScatterPlot::append(iqSample_t *data, Size_t size)
{
  for (Size_t i = 0; i < size; ++i)
    append(data[i]);
}

void SDR::TScatterPlot::append_and_refresh(iqSample_t *data, Size_t size)
{
  Q_ASSERT(Values->size() == Keys->size());
  Size_t oldSize = Keys->size(), newSize = oldSize+size;
  Keys->resize(newSize);
  Values->resize(newSize);

  for (Size_t i = 0; i < size; i++)
  {
    Size_t I = oldSize + i;
    Keys->data()[I]=data[i].i;
    Values->data()[I]=data[i].q;
  }
  send_data_to_refresh(valuesCurve, Keys, Values, true);
}
