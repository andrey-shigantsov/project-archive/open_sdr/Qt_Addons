#ifndef TPLOT_H
#define TPLOT_H

#include <SDR/Qt_Addons/common.h>
#include <qcustomplot.h>

namespace SDR
{

class TPlotItemTracer;
class TPlot : public QCustomPlot
{
  Q_OBJECT
public:
  explicit TPlot(QString type, QWidget *parent = 0);
  ~TPlot();

  QString Type(){return type;}

  QString Name(){return name;}
  void setName(QString name){this->name = name;}

  void setAspectRatio(QSize size);

  bool IsDataRefresh(){return isDataRefresh;}
  bool IsZoomEnabled(Qt::Orientations);
  bool IsDragEnabled(Qt::Orientations);

  void setAxisLabelPointSize(int pointSize);
  void setBaseColors(QColor Background, QColor Axis);

  void setRangeX(double min, double max, double diff = 0);
  void setRangeY(double min, double max, double diff = 0);

  void setAutoRangeX(bool flag, double diff = 0.1);
  void setAutoRangeY(bool flag, double diff = 0.1);

  void enableZoom(Qt::Orientations);
  void disableZoom();

  void enableDrag(Qt::Orientations);
  void disableDrag();

  virtual void append(Sample_t * samples, Size_t Count);
  virtual void append(iqSample_t * samples, Size_t Count);
  virtual void clearData() = 0;

public slots:
  void clear(){clearData();replot();}

  void readParams(QString paramsString);
  virtual bool setParam(QString &name, QString & value);

  void setAutoRefresh(bool isOn);
  void setAutoReplot(bool isOn);
  void setCollectData(bool isOn);

  void refresh_data(QCPCurve* plot, QVector<double>* keys, QVector<double>* values, bool last);
  virtual void replot();

signals:
  void refresh_started();
  void ready_for_refresh(QCPCurve *plot, QVector<double>* keys, QVector<double>* values, bool last);
  void refreshed();

protected:
  QString type, name;

  QSize aspectRatio;

  bool isDataRefresh, isAutoReplot, isAutoRefresh, isCollectData, isCustomCollectData, isAutoRangeX, isAutoRangeY;
  double scaleFactorX, scaleFactorY;

  TPlotItemTracer * phaseTracer;
  QCPItemText * phaseTracerText;

  void setType(QString type){this->type = type;}

  void setCustomCollectDataEnabled(bool flag){isCustomCollectData = flag;}

  QCPCurve* createCurve();
  QCPCurve* createCurve(QPen pen);

  void send_data_to_refresh(QCPCurve *plot, QVector<double>*& keys, QVector<double>*& values, bool last);

  virtual void autorange(QCPAxis * axis, double scaleFactor);

  void resizeEvent(QResizeEvent * event);

protected slots:
  void mousePressHandler(QMouseEvent* event);
  void curveClickHandler(QCPAbstractPlottable * plottable, int dataIndex, QMouseEvent *event);
  void updatePhaseTracer();
};

class TPlotItemTracer : public QCPAbstractItem
{
  Q_OBJECT
public:
  enum TracerStyle { tsNone        ///< The tracer is not visible
                     ,tsPlus       ///< A plus shaped crosshair with limited size
                     ,tsCrosshair  ///< A plus shaped crosshair which spans the complete axis rect
                     ,tsCircle     ///< A circle
                     ,tsSquare     ///< A square
                   };

  explicit TPlotItemTracer(QCustomPlot *parentPlot);
  virtual ~TPlotItemTracer();

  QPen pen() const { return mPen; }
  QPen selectedPen() const { return mSelectedPen; }
  QBrush brush() const { return mBrush; }
  QBrush selectedBrush() const { return mSelectedBrush; }
  double size() const { return mSize; }
  TracerStyle style() const { return mStyle; }
  QCPCurve *curve() const { return mCurve; }
  double graphKey() const { return mCurveKey; }
  bool interpolating() const { return mInterpolating; }

  void setPen(const QPen &pen);
  void setSelectedPen(const QPen &pen);
  void setBrush(const QBrush &brush);
  void setSelectedBrush(const QBrush &brush);
  void setSize(double size);
  void setStyle(TracerStyle style);
  void setCurve(QCPCurve *curve);
  void setDataIdx(int idx);
  void setCurveKey(double key);
  void setInterpolating(bool enabled);

  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;

  void updatePosition();

  QCPItemPosition * const position;
protected:
  // property members:
  QPen mPen, mSelectedPen;
  QBrush mBrush, mSelectedBrush;
  double mSize;
  TracerStyle mStyle;
  QCPCurve *mCurve;
  double mCurveKey;
  int mDataIdx;
  bool mInterpolating;

  // reimplemented virtual methods:
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;

  // non-virtual methods:
  QPen mainPen() const;
  QBrush mainBrush() const;
};

} // SDR

#endif // TPLOT_H
