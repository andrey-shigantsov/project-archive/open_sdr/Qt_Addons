#include "TSyncWindowPlot.h"

void TSyncWindowPlot::init()
{
  setCustomCollectDataEnabled(true);

  setSymbolsCount(16);

  PlotableRe = createCurve(QPen(QColor(Qt::green)));
  PlotableIm = createCurve(QPen(QColor(Qt::yellow)));

  Keys = new QVector<double>;
  ValuesRe = new QVector<double>;
  ValuesIm = new QVector<double>;

  // Начало символа
  syncWindow_SymbolStart = new QCPItemStraightLine(this);
  syncWindow_SymbolStart->setPen(QPen(Qt::white));
  // Позиция выборки символа
  syncWindow_SymbolGetPos = new QCPItemStraightLine(this);
  syncWindow_SymbolGetPos->setPen(QPen(Qt::red));
  // Конец символа
  syncWindow_SymbolStop = new QCPItemStraightLine(this);
  syncWindow_SymbolStop->setPen(QPen(Qt::white));
}

void TSyncWindowPlot::refresh_lines_pos()
{ 
  syncWindow_SymbolStart->point1->setCoords(SymStart, 0);
  syncWindow_SymbolStart->point2->setCoords(SymStart, 1);
  syncWindow_SymbolStop->point1->setCoords(SymStop, 0);
  syncWindow_SymbolStop->point2->setCoords(SymStop, 1);
  syncWindow_SymbolGetPos->point1->setCoords(SymGetPos, 0);
  syncWindow_SymbolGetPos->point2->setCoords(SymGetPos, 1);
  replot();
}

void TSyncWindowPlot::autorange(QCPAxis *axis, double scaleFactor)
{
//  if (axis == getCustomPlot()->yAxis)
//  {

//  }
//  else
  TPlot::autorange(axis, scaleFactor);
}

#define PLOT_TYPE "SyncWindow"

TSyncWindowPlot::TSyncWindowPlot(QWidget *parent) :
  TPlot(PLOT_TYPE, parent)
{
  init();

  this->SyncWindow = 0;
  this->iqSyncWindow = 0;

  SymStart = 0;
  SymStop = 0;
  SymGetPos = 0;
  refresh_lines_pos();
}

TSyncWindowPlot::TSyncWindowPlot(bool appendRawBuf, bool RawBufIsComplex, QWidget *parent) :
  TSyncWindowPlot(parent)
{
  isRawBuf = appendRawBuf;
  this->RawBufIsComplex = RawBufIsComplex;
}

TSyncWindowPlot::TSyncWindowPlot(SyncWindow_t *syncWindow, QWidget *parent) :
  TPlot(PLOT_TYPE, parent)
{
  init();
  setSyncWindow(syncWindow);
}

TSyncWindowPlot::TSyncWindowPlot(iqSyncWindow_t *syncWindow, QWidget *parent) :
  TPlot(PLOT_TYPE, parent)
{
  init();
  setSyncWindow(syncWindow);
}

TSyncWindowPlot::TSyncWindowPlot(iqSymbolSync_t *SymbolSync, QWidget *parent) :
  TPlot(PLOT_TYPE, parent)
{
  setSyncWindow(SymbolSync);
}

TSyncWindowPlot::~TSyncWindowPlot()
{
  delete Keys;
  delete ValuesRe;
  delete ValuesIm;
}

void TSyncWindowPlot::setSymLinesPos(Size_t SymStart, Size_t SymSize, Size_t SymGetPos)
{
  this->SymStart = SymStart;
  SymStop = SymStart + SymSize;
  this->SymGetPos = SymGetPos;
  refresh_lines_pos();
}

void TSyncWindowPlot::setSyncWindow(SyncWindow_t *syncWindow)
{
  this->SyncWindow = syncWindow;
  this->iqSyncWindow = 0;

  setRangeX(0,SyncWindow->Window.Size-1);

  setSymLinesPos(SyncWindow_SymbolPos(SyncWindow), SyncWindow_SymbolSize(SyncWindow), SyncWindow_SymbolPos(SyncWindow) + SyncWindow_SymbolSize(SyncWindow)/2);
}

void TSyncWindowPlot::setSyncWindow(iqSyncWindow_t *syncWindow)
{
  this->SyncWindow = 0;
  this->iqSyncWindow = syncWindow;

  setRangeX(0,iqSyncWindow->Window.Size-1);

  setSymLinesPos(iqSyncWindow_SymbolPos(iqSyncWindow), iqSyncWindow_SymbolSize(iqSyncWindow), iqSyncWindow_SymbolPos(iqSyncWindow) + iqSyncWindow_SymbolSize(iqSyncWindow)/2);
}

void TSyncWindowPlot::setSyncWindow(iqSymbolSync_t *SymbolSync)
{
  setSyncWindow(&SymbolSync->window);
  setSymbolGetOffset(iqTimingErrorDetector_SymbolCenter(&SymbolSync->ted));
}

void TSyncWindowPlot::setSymbolGetOffset(Value_t offset)
{
  SymGetPos = SymStart+offset;
  syncWindow_SymbolGetPos->point1->setCoords(SymGetPos, 0);
  syncWindow_SymbolGetPos->point2->setCoords(SymGetPos, 1);
}

void TSyncWindowPlot::setSymbolsCount(Size_t count)
{
  if (count > 1)
    setCollectData(true);
  else
    setCollectData(false);
  SymbolsCount = count;
}

void TSyncWindowPlot::raw_append(Sample_t *samples, Size_t Count)
{
  if (isCollectData)
  {
    QVector<double> Re(Count);
    for (Size_t i = 0; i < Count; ++i)
      Re.data()[i] = samples[i];
    delayRe.append(Re);
    while ((Size_t)delayRe.size() > SymbolsCount)
      delayRe.removeFirst();
    for (Size_t j = 0; j < (Size_t)delayRe.size(); ++j)
    {
      for (Size_t i = 0; i < (Size_t)delayRe.at(j).size(); ++i)
        Keys->append(i);
      ValuesRe->append(delayRe.at(j));
      Keys->append(delayRe.at(j).size());
      ValuesRe->append(NAN);
    }
  }
  else
  {
    Keys->resize(Count);
    ValuesRe->resize(Count);
    for (Size_t i = 0; i < Count; ++i)
    {
      Keys->data()[i] = i;
      ValuesRe->data()[i] = samples[i];
    }
  }
  send_data_to_refresh(PlotableRe, Keys, ValuesRe, true);
}

void TSyncWindowPlot::raw_append(iqSample_t *samples, Size_t Count)
{
  if (isCollectData)
  {
    QVector<double> Re(Count), Im(Count);
    for (Size_t i = 0; i < Count; ++i)
    {
      Re.data()[i] = samples[i].i;
      Im.data()[i] = samples[i].q;
    }
    delayRe.append(Re);
    delayIm.append(Im);
    while ((Size_t)delayRe.size() > SymbolsCount)
    {
      delayRe.removeFirst();
      delayIm.removeFirst();
    }
    for (Size_t j = 0; j < (Size_t)delayRe.size(); ++j)
    {
      for (Size_t i = 0; i < (Size_t)delayRe.at(j).size(); ++i)
        Keys->append(i);
      ValuesRe->append(delayRe.at(j));
      ValuesIm->append(delayIm.at(j));
      Keys->append(delayRe.at(j).size());
      ValuesRe->append(NAN);
      ValuesIm->append(NAN);
    }
  }
  else
  {
    Keys->resize(Count);
    ValuesRe->resize(Count);
    ValuesIm->resize(Count);
    for (Size_t i = 0; i < Count; ++i)
    {
      Keys->data()[i] = i;
      ValuesRe->data()[i] = samples[i].i;
      ValuesIm->data()[i] = samples[i].q;
    }
  }
  send_data_to_refresh(PlotableRe, Keys, ValuesRe, false);
  send_data_to_refresh(PlotableIm, Keys, ValuesIm, true);
}

void TSyncWindowPlot::append(Sample_t *samples, Size_t count)
{
  SDR_ASSERT(isRawBuf && !RawBufIsComplex);
  raw_append(samples,count);
}

void TSyncWindowPlot::append(iqSample_t *samples, Size_t count)
{
  SDR_ASSERT(isRawBuf && RawBufIsComplex);
  raw_append(samples,count);
}

void TSyncWindowPlot::update()
{
  if (SyncWindow)
    raw_append(SyncWindow_CollectorBuf(SyncWindow)->P, SyncWindow_CollectorBuf(SyncWindow)->Size);
  else if (iqSyncWindow)
    raw_append(iqSyncWindow_CollectorBuf(iqSyncWindow)->P, iqSyncWindow_CollectorBuf(iqSyncWindow)->Size);
}

void TSyncWindowPlot::clearData()
{
  Keys->clear();
  if (iqSyncWindow || RawBufIsComplex)
  {
    delayIm.clear();
    ValuesIm->clear();
    PlotableIm->data().data()->clear();
  }
  delayRe.clear();
  ValuesRe->clear();
  PlotableRe->data().data()->clear();
}

bool TSyncWindowPlot::setParam(QString &name, QString &value)
{
  if (TPlot::setParam(name,value)) return true;
  if (name == "SymLinesPos")
  {
    QStringList values = value.split(",");
    Q_ASSERT(values.count() == 3);
    setSymLinesPos(values.at(0).toInt(), values.at(1).toInt(), values.at(2).toInt());
    return true;
  }
  return false;
}
