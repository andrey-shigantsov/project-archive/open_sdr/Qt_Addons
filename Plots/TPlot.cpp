#include "TPlot.h"

#include <QStringList>

using namespace SDR;

TPlot::TPlot(QString type, QWidget *parent) :
  QCustomPlot(parent)
{
  name = name;

  xAxis->setTickLabelRotation(45);
  setAxisLabelPointSize(7);
  setBaseColors(QColor(10,10,10,255), QColor(255,255,255,200));

//  setInteraction(QCP::iSelectPlottables, true);
  connect(this, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(mousePressHandler(QMouseEvent*)));
  connect(this, SIGNAL(plottableClick(QCPAbstractPlottable*,int,QMouseEvent*)), this, SLOT(curveClickHandler(QCPAbstractPlottable*,int,QMouseEvent*)));

  phaseTracer = new TPlotItemTracer(this);
  phaseTracer->setVisible(false);
  phaseTracer->setLayer("overlay");
//  phaseTracer->setInterpolating(true);
  phaseTracer->setStyle(TPlotItemTracer::tsSquare);
  phaseTracer->setPen(QPen(QColor(Qt::green)));
  phaseTracer->setBrush(Qt::green);
  phaseTracer->setSize(3);

  phaseTracerText = new QCPItemText(this);
  phaseTracerText->setVisible(false);
  phaseTracerText->setLayer("overlay");
  phaseTracerText->setColor(QColor(Qt::black));
  phaseTracerText->setBrush(QBrush(Qt::darkGray));
  phaseTracerText->position->setType(QCPItemPosition::ptPlotCoords);
  phaseTracerText->setTextAlignment(Qt::AlignLeft);
  phaseTracerText->setFont(xAxis->tickLabelFont());

  isDataRefresh = false;
  isAutoReplot = true;
  isAutoRefresh = true;
  isCollectData = false;
  isCustomCollectData = false;
  isAutoRangeX = false;
  isAutoRangeY = false;
}

TPlot::~TPlot()
{
}

void TPlot::mousePressHandler(QMouseEvent *event)
{
  Q_UNUSED(event);
  phaseTracer->setVisible(false);
  phaseTracerText->setVisible(false);
  replot();
}

void TPlot::curveClickHandler(QCPAbstractPlottable *plottable, int dataIndex, QMouseEvent * event)
{
  Q_UNUSED(event);
  QCPCurve * curve = (QCPCurve *) plottable;

  phaseTracer->setVisible(true);
  phaseTracer->setCurve(curve);
  phaseTracer->setDataIdx(dataIndex);
  phaseTracerText->setVisible(true);
  updatePhaseTracer();
  replot();
}

void TPlot::updatePhaseTracer()
{
  if (phaseTracer->curve()->data().data()->size() == 0)
  {
    phaseTracer->setVisible(false);
    phaseTracerText->setVisible(false);
    return;
  }

  phaseTracer->updatePosition();
  phaseTracerText->setText(QString(" X:%1\n Y:%2").arg(phaseTracer->position->key()).arg(phaseTracer->position->value()));
  {
    Qt::AlignmentFlag hAl, vAl;
    if (phaseTracer->position->key() < xAxis->range().center())
    {
      hAl = Qt::AlignLeft;
    }
    else
    {
      hAl = Qt::AlignRight;
    }
    if (phaseTracer->position->value() < yAxis->range().center())
    {
      vAl = Qt::AlignBottom;
    }
    else
    {
      vAl = Qt::AlignTop;
    }
    phaseTracerText->position->setCoords(phaseTracer->position->key(), phaseTracer->position->value());
    phaseTracerText->setPositionAlignment(hAl | vAl);
  }
}

void TPlot::setAxisLabelPointSize(int pointSize)
{
  QFont Font; Font.setPointSize(pointSize);

  xAxis->setTickLabelFont(Font);
  xAxis2->setTickLabelFont(Font);
  yAxis->setTickLabelFont(Font);
  yAxis2->setTickLabelFont(Font);
}

void TPlot::setBaseColors(QColor Background, QColor Axis)
{
  setBackground(QBrush(Background));

  QPen penAxis(Axis);

  xAxis->setBasePen(penAxis);
  yAxis->setBasePen(penAxis);
  xAxis2->setBasePen(penAxis);
  yAxis2->setBasePen(penAxis);

  xAxis->setTickPen(penAxis);
  xAxis->setSubTickPen(penAxis);
  xAxis2->setTickPen(penAxis);
  xAxis2->setSubTickPen(penAxis);
  yAxis->setTickPen(penAxis);
  yAxis->setSubTickPen(penAxis);
  yAxis2->setTickPen(penAxis);
  yAxis2->setSubTickPen(penAxis);

  xAxis->setTickLabelColor(Axis);
  xAxis2->setTickLabelColor(Axis);
  yAxis->setTickLabelColor(Axis);
  yAxis2->setTickLabelColor(Axis);
}

void TPlot::setAutoRefresh(bool isOn)
{
  isAutoRefresh = isOn;
}

void TPlot::setAutoReplot(bool isOn)
{
  isAutoReplot = isOn;
}

void TPlot::setCollectData(bool isOn)
{
  isCollectData = isOn;
}

QCPCurve* TPlot::createCurve()
{
  QCPCurve* plotable = new QCPCurve(xAxis, yAxis);
  return plotable;
}

QCPCurve* TPlot::createCurve(QPen pen)
{
  QCPCurve* plotable = createCurve();
  plotable->setPen(pen);
  return plotable;
}

void TPlot::send_data_to_refresh(QCPCurve *plot, QVector<double> *&keys, QVector<double> *& values, bool last)
{
  if (isAutoRefresh)
  {
    refresh_data(plot,keys,values,last);
  }
  else
  {
    if (!isDataRefresh)
    {
      if (last)
      {
        isDataRefresh = true;
        emit refresh_started();
      }
      emit ready_for_refresh(plot,keys,values,last);
    }
  }
  if (last)
    keys = new QVector<double>;
  values = new QVector<double>;
}

void TPlot::refresh_data(QCPCurve *plot, QVector<double>* keys, QVector<double>* values, bool last)
{
  if (!isCustomCollectData && isCollectData)
    plot->addData(*keys, *values);
  else
    plot->setData(*keys, *values);

  if (last)
  {
    if (isAutoReplot)
      replot();
    isDataRefresh = false;
    emit refreshed();
  }
}

void TPlot::replot()
{
  if (isAutoRangeY && (!(IsDragEnabled(Qt::Vertical)||IsZoomEnabled(Qt::Vertical))))
    autorange(yAxis, scaleFactorY);
  if (isAutoRangeX && (!(IsDragEnabled(Qt::Horizontal)||IsZoomEnabled(Qt::Horizontal))))
    autorange(xAxis, scaleFactorX);
  if (phaseTracer->visible())
    updatePhaseTracer();
  QCustomPlot::replot();
}

void TPlot::autorange(QCPAxis *axis, double scaleFactor)
{
  double oldSize = axis->range().size();
  axis->rescale();
  if (axis->range().size() != oldSize)
    axis->scaleRange(scaleFactor);
}


void TPlot::setAspectRatio(QSize size)
{
  aspectRatio = size;
  setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Preferred));
}

void TPlot::resizeEvent(QResizeEvent *event)
{
  if (aspectRatio.isValid())
    setMinimumWidth(event->size().height()*aspectRatio.width()/aspectRatio.height());
  QCustomPlot::resizeEvent(event);
}

void TPlot::setRangeX(double min, double max, double diff)
{
  double factor = 1.0 + diff;
  xAxis->setRange(min*factor,max*factor);
}

void TPlot::setRangeY(double min, double max, double diff)
{
  double factor = 1.0 + diff;
  yAxis->setRange(min*factor,max*factor);
}

void TPlot::setAutoRangeX(bool flag, double diff)
{
  isAutoRangeX = flag;
  scaleFactorX = 1.0 + diff;
}

void TPlot::setAutoRangeY(bool flag, double diff)
{
  isAutoRangeY = flag;
  scaleFactorY = 1.0 + diff;
}

void TPlot::enableZoom(Qt::Orientations o)
{
  setInteraction(QCP::iRangeZoom, true);
  axisRect()->setRangeZoom(o);
}

void TPlot::disableZoom()
{
  setInteraction(QCP::iRangeZoom, false);
  axisRect()->setRangeZoom(Qt::Horizontal|Qt::Vertical);
}

bool TPlot::IsZoomEnabled(Qt::Orientations o)
{
  return interactions().testFlag(QCP::iRangeZoom) && ((axisRect()->rangeZoom() & o) == o);
}

bool TPlot::IsDragEnabled(Qt::Orientations o)
{
  return interactions().testFlag(QCP::iRangeDrag) && ((axisRect()->rangeDrag() & o) == o);
}

void TPlot::enableDrag(Qt::Orientations o)
{
  setInteraction(QCP::iRangeDrag, true);
  axisRect()->setRangeDrag(o);
}

void TPlot::disableDrag()
{
  setInteraction(QCP::iRangeDrag, false);
  axisRect()->setRangeDrag(Qt::Horizontal|Qt::Vertical);
}

void TPlot::append(Sample_t *samples, Size_t Count)
{
  Q_UNUSED(samples);
  Q_UNUSED(Count);
  qWarning() << QString("T%1Plot: append Samples is not supported").arg(type);
}

void TPlot::append(iqSample_t *samples, Size_t Count)
{
  Q_UNUSED(samples);
  Q_UNUSED(Count);
  qWarning() << QString("T%1Plot: append iqSamples is not supported").arg(type);
}

void TPlot::readParams(QString paramsStr)
{
  QStringList params = paramsStr.split(";");
  foreach (const QString &p, params)
  {
    if (p.isEmpty()) continue;
    QStringList x = p.split(":");
    SDR_ASSERT(x.count() == 2);
    setParam(x[0],x[1]);
  }
}

bool TPlot::setParam(QString &name, QString &value)
{
  if (name == "rangeX")
  {
    QStringList x = value.split(",");
    SDR_ASSERT(x.count() >= 2);
    if (x.count() == 2)
      setRangeX(x[0].toDouble(),x[1].toDouble());
    else if (x.count() == 3)
      setRangeX(x[0].toDouble(),x[1].toDouble(),x[2].toDouble());
    return true;
  }
  if (name == "rangeY")
  {
    QStringList x = value.split(",");
    SDR_ASSERT(x.count() >= 2);
    if (x.count() == 2)
      setRangeY(x[0].toDouble(),x[1].toDouble());
    else if (x.count() == 3)
      setRangeY(x[0].toDouble(),x[1].toDouble(),x[2].toDouble());
    return true;
  }
  //TODO: others
  return false;
}

//-------------------------------------------------------------------------------

TPlotItemTracer::TPlotItemTracer(QCustomPlot *parentPlot) :
  QCPAbstractItem(parentPlot),
  position(createPosition(QLatin1String("position"))),
  mSize(6),
  mStyle(tsCrosshair),
  mCurve(0),
  mCurveKey(0),
  mDataIdx(-1),
  mInterpolating(false)
{
  position->setCoords(0, 0);

  setBrush(Qt::NoBrush);
  setSelectedBrush(Qt::NoBrush);
  setPen(QPen(Qt::black));
  setSelectedPen(QPen(Qt::blue, 2));
}

TPlotItemTracer::~TPlotItemTracer()
{
}

void TPlotItemTracer::setPen(const QPen &pen)
{
  mPen = pen;
}

void TPlotItemTracer::setSelectedPen(const QPen &pen)
{
  mSelectedPen = pen;
}

void TPlotItemTracer::setBrush(const QBrush &brush)
{
  mBrush = brush;
}

void TPlotItemTracer::setSelectedBrush(const QBrush &brush)
{
  mSelectedBrush = brush;
}

void TPlotItemTracer::setSize(double size)
{
  mSize = size;
}

void TPlotItemTracer::setStyle(TPlotItemTracer::TracerStyle style)
{
  mStyle = style;
}

void TPlotItemTracer::setCurve(QCPCurve *curve)
{
  if (curve)
  {
    if (curve->parentPlot() == mParentPlot)
    {
      position->setType(QCPItemPosition::ptPlotCoords);
      position->setAxes(curve->keyAxis(), curve->valueAxis());
      mCurve = curve;
      updatePosition();
    } else
      qDebug() << Q_FUNC_INFO << "graph isn't in same QCustomPlot instance as this item";
  } else
  {
    mCurve = 0;
  }
}

void TPlotItemTracer::setDataIdx(int idx)
{
  mDataIdx = idx;
}

void TPlotItemTracer::setCurveKey(double key)
{
  mDataIdx = -1;
  mCurveKey = key;
}

void TPlotItemTracer::setInterpolating(bool enabled)
{
  mInterpolating = enabled;
}

double TPlotItemTracer::selectTest(const QPointF &pos, bool onlySelectable, QVariant *details) const
{
  Q_UNUSED(details)
  if (onlySelectable && !mSelectable)
    return -1;

  QPointF center(position->pixelPosition());
  double w = mSize/2.0;
  QRect clip = clipRect();
  switch (mStyle)
  {
    case tsNone: return -1;
    case tsPlus:
    {
      if (clipRect().intersects(QRectF(center-QPointF(w, w), center+QPointF(w, w)).toRect()))
        return qSqrt(qMin(QCPVector2D(pos).distanceSquaredToLine(center+QPointF(-w, 0), center+QPointF(w, 0)),
                          QCPVector2D(pos).distanceSquaredToLine(center+QPointF(0, -w), center+QPointF(0, w))));
      break;
    }
    case tsCrosshair:
    {
      return qSqrt(qMin(QCPVector2D(pos).distanceSquaredToLine(QCPVector2D(clip.left(), center.y()), QCPVector2D(clip.right(), center.y())),
                        QCPVector2D(pos).distanceSquaredToLine(QCPVector2D(center.x(), clip.top()), QCPVector2D(center.x(), clip.bottom()))));
    }
    case tsCircle:
    {
      if (clip.intersects(QRectF(center-QPointF(w, w), center+QPointF(w, w)).toRect()))
      {
        // distance to border:
        double centerDist = QCPVector2D(center-pos).length();
        double circleLine = w;
        double result = qAbs(centerDist-circleLine);
        // filled ellipse, allow click inside to count as hit:
        if (result > mParentPlot->selectionTolerance()*0.99 && mBrush.style() != Qt::NoBrush && mBrush.color().alpha() != 0)
        {
          if (centerDist <= circleLine)
            result = mParentPlot->selectionTolerance()*0.99;
        }
        return result;
      }
      break;
    }
    case tsSquare:
    {
      if (clip.intersects(QRectF(center-QPointF(w, w), center+QPointF(w, w)).toRect()))
      {
        QRectF rect = QRectF(center-QPointF(w, w), center+QPointF(w, w));
        bool filledRect = mBrush.style() != Qt::NoBrush && mBrush.color().alpha() != 0;
        return rectDistance(rect, pos, filledRect);
      }
      break;
    }
  }
  return -1;
}

void TPlotItemTracer::draw(QCPPainter *painter)
{
  updatePosition();
  if (mStyle == tsNone)
    return;

  painter->setPen(mainPen());
  painter->setBrush(mainBrush());
  QPointF center(position->pixelPosition());
  double w = mSize/2.0;
  QRect clip = clipRect();
  switch (mStyle)
  {
    case tsNone: return;
    case tsPlus:
    {
      if (clip.intersects(QRectF(center-QPointF(w, w), center+QPointF(w, w)).toRect()))
      {
        painter->drawLine(QLineF(center+QPointF(-w, 0), center+QPointF(w, 0)));
        painter->drawLine(QLineF(center+QPointF(0, -w), center+QPointF(0, w)));
      }
      break;
    }
    case tsCrosshair:
    {
      if (center.y() > clip.top() && center.y() < clip.bottom())
        painter->drawLine(QLineF(clip.left(), center.y(), clip.right(), center.y()));
      if (center.x() > clip.left() && center.x() < clip.right())
        painter->drawLine(QLineF(center.x(), clip.top(), center.x(), clip.bottom()));
      break;
    }
    case tsCircle:
    {
      if (clip.intersects(QRectF(center-QPointF(w, w), center+QPointF(w, w)).toRect()))
        painter->drawEllipse(center, w, w);
      break;
    }
    case tsSquare:
    {
      if (clip.intersects(QRectF(center-QPointF(w, w), center+QPointF(w, w)).toRect()))
        painter->drawRect(QRectF(center-QPointF(w, w), center+QPointF(w, w)));
      break;
    }
  }
}

void TPlotItemTracer::updatePosition()
{
  if (mCurve)
  {
    if (mParentPlot->hasPlottable(mCurve))
    {
      if (mCurve->data()->size() > 1)
      {
        if (mDataIdx == -1)
        {
          QCPCurveDataContainer::const_iterator first = mCurve->data()->constBegin();
          QCPCurveDataContainer::const_iterator last = mCurve->data()->constEnd()-1;
          if (mCurveKey <= first->key)
            position->setCoords(first->key, first->value);
          else if (mCurveKey >= last->key)
            position->setCoords(last->key, last->value);
          else
          {
            QCPCurveDataContainer::const_iterator it = mCurve->data()->findBegin(mCurveKey);
            if (it != mCurve->data()->constEnd()) // mGraphKey is not exactly on last iterator, but somewhere between iterators
            {
              QCPCurveDataContainer::const_iterator prevIt = it;
              ++it; // won't advance to constEnd because we handled that case (mGraphKey >= last->key) before
              if (mInterpolating)
              {
                // interpolate between iterators around mGraphKey:
                double slope = 0;
                if (!qFuzzyCompare((double)it->key, (double)prevIt->key))
                  slope = (it->value-prevIt->value)/(it->key-prevIt->key);
                position->setCoords(mCurveKey, (mCurveKey-prevIt->key)*slope+prevIt->value);
              } else
              {
                // find iterator with key closest to mGraphKey:
                if (mCurveKey < (prevIt->key+it->key)*0.5)
                  position->setCoords(prevIt->key, prevIt->value);
                else
                  position->setCoords(it->key, it->value);
              }
            } else // mGraphKey is exactly on last iterator (should actually be caught when comparing first/last keys, but this is a failsafe for fp uncertainty)
              position->setCoords(it->key, it->value);
          }
        }
        else
        {
          QCPCurveDataContainer::const_iterator it = mCurve->data().data()->at(mDataIdx);
          position->setCoords(it->key, it->value);
        }
      } else if (mCurve->data()->size())
      {
        QCPCurveDataContainer::const_iterator it = mCurve->data()->constBegin();
        position->setCoords(it->key, it->value);
      } else
        qDebug() << Q_FUNC_INFO << "graph has no data";
    } else
      qDebug() << Q_FUNC_INFO << "graph not contained in QCustomPlot instance (anymore)";
  }
}

QPen TPlotItemTracer::mainPen() const
{
  return mSelected ? mSelectedPen : mPen;
}

QBrush TPlotItemTracer::mainBrush() const
{
  return mSelected ? mSelectedBrush : mBrush;
}
