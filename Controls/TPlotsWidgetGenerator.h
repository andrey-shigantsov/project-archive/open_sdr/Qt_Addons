#ifndef TPLOTSWIDGETGENERATOR_H
#define TPLOTSWIDGETGENERATOR_H

#include <QObject>
#include <QMap>
#include <QJsonObject>

#include <SDR/Qt_Addons/Controls/TPlotsWidget.h>

namespace SDR
{

class TPlotsWidgetGenerator : public QObject
{
  Q_OBJECT
public:
  explicit TPlotsWidgetGenerator(QMap<int,TPlot*> * map = 0, QObject *parent = nullptr);

  static TPlotsWidget * create(const char * jsonFormat, QWidget * parent = 0, QMap<int,TPlot*> * map = 0);
  static bool read(const char * jsonFormat, TPlotsWidget * pw = 0, QMap<int,TPlot*> * map = 0);

signals:

public slots:

private:
  QMap<int,TPlot*> * PlotsMap;

  void read(QJsonObject & json, TPlotsWidget * pw);
  TPlotsWidget * read_new(QJsonObject & json, QWidget * Parent = nullptr);

  void read_tab(QJsonObject & json, TPlotsWidget * pw, bool isFirst);
  void read_object(QJsonObject & json, TPlotsWidget * pw);
  void read_container(QJsonObject & json, TPlotsWidget * pw);

  void read_Scope(QJsonObject & json, TPlotsWidget * pw);
  void read_Spectrum(QJsonObject & json, TPlotsWidget * pw);
  void read_Vector(QJsonObject &json, TPlotsWidget *pw);
  void read_Scatter(QJsonObject &json, TPlotsWidget *pw);
  void read_SyncWindow(QJsonObject &json, TPlotsWidget *pw);

  void read_PlotBase(QJsonObject & json, TPlot * p, TPlotsWidget *pw);
  void read_PlotRange(const QString name, QJsonObject & json, TPlot * p);
};

}

#endif // TPLOTSWIDGETGENERATOR_H
