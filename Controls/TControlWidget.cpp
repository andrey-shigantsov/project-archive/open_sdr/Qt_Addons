#include "TControlWidget.h"

#include <QBoxLayout>
#include <QGroupBox>
#include <QTabWidget>
#include <QLabel>
#include <QSpinBox>
#include <QDoubleSpinBox>

#include <QDebug>

using namespace SDR;

TControlWidget::TControlWidget(QWidget *parent) : TControlWidget(QBoxLayout::TopToBottom, parent){}
TControlWidget::TControlWidget(QBoxLayout::Direction dir, QWidget *parent):
  QWidget(parent), Layout(dir,this)
{
  Layout.setContentsMargins(0,0,0,0);
  LayoutStack.append(&Layout);
}

void TControlWidget::setDirection(QBoxLayout::Direction dir)
{
  Layout.setDirection(dir);
}

QBoxLayout * TControlWidget::createNewLayout(QBoxLayout::Direction dir)
{
  QBoxLayout * curLayout = new QBoxLayout(dir);
  curLayout->setContentsMargins(3,3,3,3);
  return curLayout;
}

void TControlWidget::BeginContaner(bool isTab, QBoxLayout::Direction dir, QString name, int stretch)
{
  if ((isTab)&&(TabWidgetStack.size() < 1))
  {
    qWarning() << "SDR::TControlWidget:" << "ignore tab for empty tab-container";
    return;
  }
  QBoxLayout * curLayout = createNewLayout(dir);
  Q_ASSERT(LayoutStack.size() > 0);
  Q_ASSERT (isTab || (!isTab && LayoutStack.last()));

  if (isTab)
  {
    if (LayoutStack.last() == nullptr)
      LayoutStack.removeLast();
    QWidget * container = new QWidget();
    TabWidgetStack.last()->addTab(container, name);
    container->setLayout(curLayout);
  }
  else if(name.isEmpty())
  {
    QWidget * container = new QWidget();
    LayoutStack.last()->addWidget(container,stretch);
    container->setLayout(curLayout);
  }
  else
  {
    QGroupBox * container = new QGroupBox(name);
    container->setLayout(curLayout);
    LayoutStack.last()->addWidget(container,stretch);
  }
  LayoutStack.append(curLayout);
}

void TControlWidget::BeginContaner(QBoxLayout::Direction dir, QString name, int stretch)
{
  BeginContaner(false,dir,name,stretch);
}

void TControlWidget::BeginTabContainer(QBoxLayout::Direction dir, QString name)
{
  BeginContaner(true,dir,name);
}

void TControlWidget::EndContainer()
{
  Q_ASSERT(LayoutStack.size() > 1);
  LayoutStack.removeLast();
}

void TControlWidget::BeginTabWidget(int stretch)
{
  QTabWidget * tabContainer = new QTabWidget();
  TabWidgetStack.append(tabContainer);

  LayoutStack.last()->addWidget(tabContainer,stretch);
  LayoutStack.append(nullptr);
}

void TControlWidget::EndTabWidget()
{
  Q_ASSERT(TabWidgetStack.size() > 0);
  TabWidgetStack.removeLast();
}

void TControlWidget::appendToCurrentContainer(Control::TElement *element, int stretch)
{
  Q_ASSERT(LayoutStack.size() > 0);

  if (LayoutStack.last() == nullptr)
  {
    qWarning() << "SDR::TControlWidget:" << "ignore element for empty container";
    return;
  }

  LayoutStack.last()->addWidget(element,stretch);

  Elements[element->Id()] = element;

  connect(element, SIGNAL(valueChanged(int,QString)), this, SLOT(value_handler(int,QString)));
}

void TControlWidget::appendStretchToCurrentContainer(int stretch)
{
  Q_ASSERT(LayoutStack.size() > 0);

  if (LayoutStack.last() == nullptr)
  {
    qWarning() << "SDR::TControlWidget:" << "ignore stretch for empty container";
    return;
  }

  LayoutStack.last()->addStretch(stretch);
}

void TControlWidget::setIsStarted(bool flag)
{
  foreach (Control::TElement * el, Elements)
    el->setStarted(flag);
}

QString TControlWidget::Value(const int id)
{
  return Elements[id]->Value();
}

void TControlWidget::setValue(const int id, const QString &text)
{
  if (!Elements.contains(id))
    return;
  Elements[id]->setValue(text);
}

void TControlWidget::setParams(const int id, const QString &text)
{
  if (!Elements.contains(id))
    return;
  Elements[id]->readParams(text);
}

void TControlWidget::sendNeededValues()
{
  foreach (Control::TElement * el, Elements)
  {
    if (el->NeedRefreshValue())
      el->RefreshAndSendValue();
  }
}

void TControlWidget::sendAllValues()
{
  foreach (Control::TElement * el, Elements)
  {
    el->RefreshAndSendValue();
  }
}
