#include "ArrayInputForm.h"
#include "ui_ArrayInputForm.h"

#include <SDR/Qt_Addons/common.h>

using namespace SDR;

ArrayInputForm::ArrayInputForm(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::ArrayInputForm)
{
  ui->setupUi(this);
  isCanceled = false;
}

ArrayInputForm::~ArrayInputForm()
{
  delete ui;
}

bool ArrayInputForm::editSamples(QString &vectorStr, QVector<Sample_t> &vector, QString title)
{
  ArrayInputForm f;
  if (!title.isEmpty())
    f.setWindowTitle(title);
  if (vectorStr.isEmpty())
    vectorStr = SamplesToString(vector);
  f.ui->TextEdit->setPlainText(vectorStr);
  f.exec();
  if (f.isCanceled) return false;
  vectorStr = f.ui->TextEdit->toPlainText();
  vector = SamplesFromString(vectorStr);
  return true;
}

void ArrayInputForm::on_OK_clicked()
{
  isCanceled = false;
  close();
}

void ArrayInputForm::on_Cancel_clicked()
{
  isCanceled = true;
  close();
}
