#include "TPlotsWidget.h"

using namespace SDR;

TPlotsWidget::TPlotsWidget(QWidget *parent, QBoxLayout::Direction dir) :
  QTabWidget(parent)
{
  tabBar()->hide();
  QTabWidget::addTab(new QWidget(this), "");
  currentWidget()->setLayout(createLayout(dir));

  plotMinW = plotMinH = 128;
}

TPlotsWidget::TPlotsWidget(QStringList & tabNames, QList<QBoxLayout::Direction> & tabDirs, QWidget *parent) :
  QTabWidget(parent)
{
  Q_ASSERT(tabNames.count() == tabDirs.count());
  for (int i = 0; i < tabNames.count(); ++i)
    addTab(tabNames.at(i), tabDirs.at(i));
  setCurrentIndex(0);
}

TPlotsWidget::~TPlotsWidget()
{
}

void TPlotsWidget::setPlotMinimumSize(int w, int h)
{
  plotMinW = w;
  plotMinH = h;
}

void TPlotsWidget::setDirection(int idx, QBoxLayout::Direction dir)
{
  Layout(idx)->setDirection(dir);
}

int TPlotsWidget::addTab(QString name, QBoxLayout::Direction dir)
{
  if (tabBar()->isHidden())
    tabBar()->show();

  QWidget * w = new QWidget(this);
  w->setLayout(createLayout(dir));
  int idx = QTabWidget::addTab(w, name);
  setCurrentIndex(idx);
  return idx;
}

void TPlotsWidget::add(int tabIdx, TPlot *plot, int stretch)
{
  Plots.append(plot);
  if (!plot->minimumWidth())
    plot->setMinimumWidth(plotMinW);
  if (!plot->minimumHeight())
    plot->setMinimumHeight(plotMinH);
  Layout(tabIdx)->addWidget(plot, stretch);
}

void TPlotsWidget::add(int tabIdx, TPlotsWidget *widget, int stretch)
{
  PlotsWidgets.append(widget);
  Layout(tabIdx)->addWidget(widget, stretch);
}

void TPlotsWidget::clearData()
{
  foreach (TPlot * p, Plots)
    p->clear();

  foreach (TPlotsWidget * pw, PlotsWidgets)
    pw->clearData();
}

void TPlotsWidget::clear()
{
  for (int i = 0; i < count(); ++i)
  {
    QBoxLayout * l = Layout(i);
    for (int j = 0; j < l->count(); ++j)
    {
      QLayoutItem * item = l->itemAt(j);
      QWidget * w = item->widget();
      l->removeItem(item);
      w->setParent(nullptr);
    }
    if (i > 0) removeTab(i);
  }

  foreach (TPlot * p, Plots)
    delete p;
  Plots.clear();

  foreach (TPlotsWidget * pw, PlotsWidgets)
    delete pw;
  PlotsWidgets.clear();

  updateGeometry();
}

void TPlotsWidget::enableZoom(Qt::Orientations o)
{
  foreach (TPlot* plot, Plots)
    plot->enableZoom(o);

  foreach (TPlotsWidget* widget, PlotsWidgets)
    widget->enableZoom(o);
}

void TPlotsWidget::disableZoom()
{
  foreach (TPlot* plot, Plots)
    plot->disableZoom();

  foreach (TPlotsWidget* widget, PlotsWidgets)
    widget->disableZoom();
}

void TPlotsWidget::enableDrag(Qt::Orientations o)
{
  foreach (TPlot* plot, Plots)
    plot->enableDrag(o);

  foreach (TPlotsWidget* widget, PlotsWidgets)
    widget->enableDrag(o);
}

void TPlotsWidget::disableDrag()
{
  foreach (TPlot* plot, Plots)
    plot->disableDrag();

  foreach (TPlotsWidget* widget, PlotsWidgets)
    widget->disableDrag();
}

QBoxLayout *TPlotsWidget::createLayout(QBoxLayout::Direction dir)
{
  QBoxLayout * l = new QBoxLayout(dir);
  l->setContentsMargins(0,0,0,0);
  l->setSpacing(0);
  return l;
}
