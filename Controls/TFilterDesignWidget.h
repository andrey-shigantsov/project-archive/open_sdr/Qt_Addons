#ifndef TFILTERDESIGNWIDGET_H
#define TFILTERDESIGNWIDGET_H

#include <QWidget>
#include <QLayout>
#include <QComboBox>
#include <QStackedLayout>
#include <QDoubleSpinBox>

#include <SDR/Qt_Addons/common.h>

namespace Ui {
class TFilterDesignWidget;
}

namespace SDR
{

class TFilterDesignWidget : public QWidget
{
  Q_OBJECT

public:
  enum TypeId {FIR};
  enum firTypeId {RawCoeffs, Window, ParksMcClellan};
  enum firWindowTypeId {Kaiser};

  static const firTypeId defaultFirTypeId = Window;

  enum freqUnits {Norm, Hz, kHz, MHz, GHz};
  enum magUnits {dB, Linear};

  explicit TFilterDesignWidget(QWidget *parent = nullptr);
  ~TFilterDesignWidget();

  bool isDesign(){return (type() == FIR)&&(firType() != RawCoeffs);}

  void setEditable(bool flag);

  TypeId type();
  void setType(TypeId id);

  firTypeId firType();
  void setFirType(firTypeId id);

  firWindowTypeId firWindowType();
  void setFirWindowType(firWindowTypeId id);

  void setCoeffs(const QVector<Sample_t> buf);
  void setCoeffs(const QString text);

  void setCoeffsCount(const int n);

  void setFsEnabled(bool flag);

  void setFs(double Hz);
  void setFc(double Hz);
  void setAs(double dB);
  void setGain(double dB);

  bool setBandsPMC(const QVector<float> bands);
  bool setDesiredRespPMC(const QVector<float> des);

  const QVector<Sample_t> coeffs(){return CoeffsBuf;}

  QString coeffsString(){return SamplesToString(CoeffsBuf);}

  void raw_design();

signals:
  void coeffsChanged();

public slots:
  void design();

protected:
  QVector<Sample_t> CoeffsBuf;

private:
  Ui::TFilterDesignWidget *ui;

  freqUnits oldfUnits, fUnitsBeforeNorm;
  magUnits oldmUnits;

  QString freqUnitsString(freqUnits units);

  freqUnits currentFreqUnits();
  magUnits currentMagUnits();

  double freq_factor();
  double freq_factor(freqUnits units);
  double freq_convert(freqUnits inUnits, double freq, freqUnits outUnits);
  double freq_convert(freqUnits inUnits, double freq);
  double freq_cutoff(freqUnits outUnits);

  double mag_convert(magUnits inUnits, double mag, magUnits outUnits);
  double mag_convert(magUnits inUnits, double mag);
  double mag_stopband(magUnits outUnits);
  double mag_gain(magUnits outUnits);

  void ui_refresh_spinbox(QDoubleSpinBox * box, double min, double max, double step, int decimals, QString suff);
  void ui_refresh_freq_spinbox(QDoubleSpinBox * box, double min, double max, double step, int decimals, QString suff, freqUnits oldUnits);
  void ui_refresh_mag_spinbox(QDoubleSpinBox * box, double min, double max, double step, int decimals, QString suff);

  enum pmcUniParamsColumnId{pmcFstart = 0, pmcFstop, pmcDes, pmcW, pmcWType, pmcColumnCount};
  QString pmcUniParamsColumnName(pmcUniParamsColumnId id);
  QString pmcUniParamsColumnHeader(pmcUniParamsColumnId id);
  QString pmcUniParamsColumnHeader(pmcUniParamsColumnId id, QString suff);

  void ui_init_pmc_cell_value(int row, pmcUniParamsColumnId id, QString value);
  void ui_set_pmc_cell_value(int row, pmcUniParamsColumnId id, QString value);
  QString ui_pmc_cell_value(int row, pmcUniParamsColumnId id);
  void ui_refresh_pmc_freq(int row, pmcUniParamsColumnId id, freqUnits oldUnits);
  void ui_refresh_pmc_freqs(QString suff, freqUnits oldUnits);

  void ui_init_pmc_row(int row);

  void ui_set_pcm_num_bands(int num);

private slots:
  void ui_init_values();
  void ui_reset_indexes();

  void ui_refresh_type();
  void ui_refresh_fir_type();
  void ui_refresh_fir_window_type();
  void ui_refresh_fir_pmc_uniparams();

  void ui_refresh_freq_units();
  void ui_refresh_mag_units();

  void ui_refresh_coeffs();

private slots:
  void design_fir();
  void design_fir_window();
  void design_fir_pmc();

private slots:
  void on_setCoeffsButton_clicked();
  void on_firPMCNumBands_editingFinished();
};

} // SDR

#endif // TFILTERDESIGNWIDGET_H
