#include "TPlotsWidgetGenerator.h"

#include <SDR/Qt_Addons/Plots/TScopePlot.h>
#include <SDR/Qt_Addons/Plots/TSpectrumPlot.h>
#include <SDR/Qt_Addons/Plots/TScatterPlot.h>
#include <SDR/Qt_Addons/Plots/TBufferPlot.h>
#include <SDR/Qt_Addons/Plots/TSyncWindowPlot.h>

#include <SDR/Qt_Addons/Controls/etc/JsonHelpers.h>

using namespace SDR;

TPlotsWidgetGenerator::TPlotsWidgetGenerator(QMap<int, TPlot *> *map, QObject *parent) : QObject(parent)
{
  PlotsMap = map;
}

TPlotsWidget * TPlotsWidgetGenerator::create(const char *jsonFormat, QWidget * parent, QMap<int, TPlot *> *map)
{
  TPlotsWidgetGenerator gen(map);
  QJsonObject json = jsonParser::parse(jsonFormat);
  if (json.isEmpty()) return 0;
  return gen.read_new(json, parent);
}

bool TPlotsWidgetGenerator::read(const char *jsonFormat, TPlotsWidget *pw, QMap<int, TPlot *> *map)
{
  TPlotsWidgetGenerator gen(map);
  QJsonObject json = jsonParser::parse(jsonFormat);
  if (json.isEmpty()) return false;
  gen.read(json, pw);
  return true;
}

void TPlotsWidgetGenerator::read(QJsonObject &json, TPlotsWidget *pw)
{
  pw->clear();
  if (json.contains("objects"))
  {
    pw->setDirection(jsonParser::direction(json));

    if (!json["objects"].isArray()) return;
    QJsonArray jsonObjs = json["objects"].toArray();
    for (int i = 0; i < jsonObjs.size(); ++i)
    {
      QJsonObject obj = jsonObjs[i].toObject();
      read_object(obj, pw);
    }
  }
  else if (json.contains("tabs"))
  {
    if (!json["tabs"].isArray()) return;
    QJsonArray jsonTabs = json["tabs"].toArray();
    for (int i = 0; i < jsonTabs.size(); ++i)
    {
      QJsonObject tab = jsonTabs[i].toObject();
      read_tab(tab, pw, i==0);
    }
  }
}

TPlotsWidget *TPlotsWidgetGenerator::read_new(QJsonObject &json, QWidget *Parent)
{
  TPlotsWidget * pw = new TPlotsWidget(Parent);
  read(json, pw);
  return pw;
}

void TPlotsWidgetGenerator::read_tab(QJsonObject &json, TPlotsWidget *pw, bool isFirst)
{
  if (json.contains("tab") && json["tab"].isObject())
  {
    QJsonObject obj = json["tab"].toObject();

    QString name = obj["name"].toString();
    if (!isFirst)
      pw->addTab(name);
    else
      pw->setTabParams(name);
    read_container(obj, pw);
  }
}

void TPlotsWidgetGenerator::read_object(QJsonObject &json, TPlotsWidget * pw)
{
  if (json.contains("container") && json["container"].isObject())
  {
    QJsonObject obj = json["container"].toObject();
    read_container(obj, pw);
  }
  else if (json.contains("Scope") && json["Scope"].isObject())
  {
    QJsonObject obj = json["Scope"].toObject();
    read_Scope(obj, pw);
  }
  else if (json.contains("Spectrum") && json["Spectrum"].isObject())
  {
    QJsonObject obj = json["Spectrum"].toObject();
    read_Spectrum(obj, pw);
  }
  else if (json.contains("Vector") && json["Vector"].isObject())
  {
    QJsonObject obj = json["Vector"].toObject();
    read_Vector(obj, pw);
  }
  else if (json.contains("Scatter") && json["Scatter"].isObject())
  {
    QJsonObject obj = json["Scatter"].toObject();
    read_Scatter(obj, pw);
  }
  else if (json.contains("SyncWindow") && json["SyncWindow"].isObject())
  {
    QJsonObject obj = json["SyncWindow"].toObject();
    read_SyncWindow(obj, pw);
  }
}

void TPlotsWidgetGenerator::read_container(QJsonObject &json, TPlotsWidget *pw)
{
  TPlotsWidget * container = read_new(json);

  int stretch = jsonParser::stretch(json);

  pw->add(container, stretch);
}

void TPlotsWidgetGenerator::read_Scope(QJsonObject &json, TPlotsWidget *pw)
{
  TScopePlot::EMode mode;
  if (json.contains("mode") && json["mode"].isString())
  {
    QString modeStr = json["mode"].toString();
    if (modeStr == "complex")
      mode = TScopePlot::mComplex;
    else if(modeStr == "real")
      mode = TScopePlot::mReal;
    else if(modeStr == "bits")
      mode = TScopePlot::mBits;
    else
    {
      qDebug() << "TPlotsWidgetGenerator: invalid Scope mode";
      return;
    }
  }
  else
  {
    qDebug() << "TPlotsWidgetGenerator: read Scope mode failure";
    return;
  }

  double SampleTime;
  if (json.contains("fs") && json["fs"].isDouble())
    SampleTime = 1.0/json["fs"].toDouble();
  else
    SampleTime = 1.0;

  TScopePlot * p = new TScopePlot(mode,SampleTime,pw);
  read_PlotBase(json, p, pw);

  if (json.contains("refreshInterval") && json["refreshInterval"].isDouble())
    p->setRefreshTime(json["refreshInterval"].toDouble());
  if (json.contains("timeInterval") && json["timeInterval"].isDouble())
    p->setTimeRange(json["timeInterval"].toDouble());
}

void TPlotsWidgetGenerator::read_Spectrum(QJsonObject &json, TPlotsWidget *pw)
{
  Frequency_t Fs;
  if (json.contains("fs") && json["fs"].isDouble())
    Fs = json["fs"].toDouble();
  else
    Fs = 1.0;

  Size_t fftN;
  if (json.contains("fftN") && json["fftN"].isDouble())
    fftN = json["fftN"].toInt();
  else
    fftN = 1024;

  TSpectrumPlot * p = new TSpectrumPlot(Fs,fftN,pw);
  read_PlotBase(json, p, pw);
}

void TPlotsWidgetGenerator::read_Vector(QJsonObject &json, TPlotsWidget *pw)
{
  TBufferPlot::EType type;
  if (json.contains("type") && json["type"].isString())
  {
    QString typeStr = json["type"].toString();
    if (typeStr == "complex")
      type = TBufferPlot::typeBufferIQ;
    else if(typeStr == "real")
      type = TBufferPlot::typeBuffer;
    else if(typeStr == "freq")
      type = TBufferPlot::typeFreqTwoSided;
    else
    {
      qDebug() << "TPlotsWidgetGenerator: invalid Vector type";
      return;
    }
  }
  else
  {
    qDebug() << "TPlotsWidgetGenerator: read Vector type failure";
    return;
  }

  TBufferPlot * p = new TBufferPlot(type,pw);
  read_PlotBase(json, p, pw);

  if (json.contains("fs") && json["fs"].isDouble())
    p->setFreqType(json["fs"].toDouble());
}

void TPlotsWidgetGenerator::read_Scatter(QJsonObject &json, TPlotsWidget *pw)
{
  TScatterPlot * p = new TScatterPlot(pw);
  read_PlotBase(json, p, pw);

  if (json.contains("SymbolsCount") && json["SymbolsCount"].isDouble())
    p->setSymbolsCount(json["SymbolsCount"].toInt());
}

void TPlotsWidgetGenerator::read_SyncWindow(QJsonObject &json, TPlotsWidget *pw)
{
  bool isComplex = true;
  if (json.contains("isReal") && json["isReal"].isBool())
    isComplex = !json["isReal"].toBool();
  if (json.contains("isComplex") && json["isComplex"].isBool())
    isComplex = json["isComplex"].toBool();

  TSyncWindowPlot * p = new TSyncWindowPlot(true,isComplex,pw);
  read_PlotBase(json, p, pw);

  if (json.contains("SymbolsCount") && json["SymbolsCount"].isDouble())
    p->setSymbolsCount(json["SymbolsCount"].toInt());
}

void TPlotsWidgetGenerator::read_PlotBase(QJsonObject &json, TPlot *p, TPlotsWidget *pw)
{
  if (json.contains("name") && json["name"].isString())
    p->setName(json["name"].toString());

  if (json.contains("id") && json["id"].isDouble())
    if (PlotsMap)
    {
      int id = json["id"].toInt();
      PlotsMap->insertMulti(id, p);
    }

  if (json.contains("rangeY"))
    read_PlotRange("rangeY", json, p);
  if (json.contains("rangeX"))
    read_PlotRange("rangeX", json, p);

  if (json.contains("autoRangeY") && json["autoRangeY"].isBool())
    p->setAutoRangeY(json["autoRangeY"].toBool());
  if (json.contains("autoRangeX") && json["autoRangeX"].isBool())
    p->setAutoRangeX(json["autoRangeX"].toBool());

  if (json.contains("collectData") && json["collectData"].isBool())
    p->setCollectData(json["collectData"].toBool());

  pw->add(p, jsonParser::stretch(json));
}

void TPlotsWidgetGenerator::read_PlotRange(const QString name, QJsonObject &json, TPlot *p)
{
  QJsonValue jRange = json[name];
  if(jRange.isArray())
  {
    QJsonArray range = jRange.toArray();
    if (!((range.size() > 1)&&(range.size() <= 3)))
    {
      qWarning() << "TPlotsWidgetGenerator: plot range is invalid";
      return;
    }
    double min = range[0].toDouble(-1), max = range[1].toDouble(-1), diff = range[2].toDouble(0);
    if (name == "rangeY")
      p->setRangeY(min,max,diff);
    else if (name == "rangeX")
      p->setRangeX(min,max,diff);
  }
}
