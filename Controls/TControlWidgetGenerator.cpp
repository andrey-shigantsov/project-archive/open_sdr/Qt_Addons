#include "TControlWidgetGenerator.h"

#include <SDR/Qt_Addons/Controls/etc/JsonHelpers.h>

using namespace SDR;

TControlWidgetGenerator::TControlWidgetGenerator(TControlWidget *cw, QObject *parent) : QObject(parent)
{
  this->cw = cw;
}

TControlWidget *TControlWidgetGenerator::create(const char *jsonFormat, QWidget *parent)
{
  QJsonObject json = jsonParser::parse(jsonFormat);
  if (json.isEmpty()) return nullptr;

  TControlWidget *cw = new TControlWidget(jsonParser::direction(json), parent);

  TControlWidgetGenerator g(cw);
  g.read_root_contant(json);
  return cw;
}

bool TControlWidgetGenerator::read(const char *jsonFormat, TControlWidget *cw)
{
  QJsonObject json = jsonParser::parse(jsonFormat);
  return read(json,cw);
}

bool TControlWidgetGenerator::read(QJsonObject &json, TControlWidget *cw)
{
  if (json.isEmpty()) return false;
  cw->setDirection(jsonParser::direction(json));

  TControlWidgetGenerator g(cw);
  return g.read_root_contant(json);
}

bool TControlWidgetGenerator::read_root_contant(QJsonObject &json)
{
  read_defaults(json);
  if (read_elements(json))
    return true;
  if (read_tabs(json))
    return true;
  return false;
}

bool TControlWidgetGenerator::read_defaults(QJsonObject &json)
{
  if (!(json.contains("defaults") && json["defaults"].isArray())) return false;
  QJsonArray jsonDefaults = json["defaults"].toArray();
  for (int i = 0; i < jsonDefaults.size(); ++i)
  {
    QJsonObject obj = jsonDefaults[i].toObject();
    read_default(obj);
  }
  return true;
}

bool TControlWidgetGenerator::read_elements(QJsonObject &json)
{
  if (!(json.contains("elements") && json["elements"].isArray())) return false;
  QJsonArray jsonElems = json["elements"].toArray();
  for (int i = 0; i < jsonElems.size(); ++i)
  {
    QJsonObject obj = jsonElems[i].toObject();
    read_element(obj);
  }
  return true;
}

void TControlWidgetGenerator::read_default(QJsonObject &json)
{
  if (json.contains("value") && json["value"].isObject())
  {
    QJsonObject obj = json["value"].toObject();
    read_ValueElementDefaults(obj);
  }
}

void TControlWidgetGenerator::read_element(QJsonObject &json)
{
  if (read_tabs(json))
    return;

  if (json.contains("container") && json["container"].isObject())
  {
    QJsonObject obj = json["container"].toObject();
    read_container(obj);
  }
  else if (json.contains("stretch") && json["stretch"].isDouble())
  {
    cw->appendStretchToCurrentContainer(json["stretch"].toInt(0));
  }
  else if (json.contains("value") && json["value"].isObject())
  {
    QJsonObject obj = json["value"].toObject();
    read_ValueElement(obj);
  }
  else if (json.contains("list") && json["list"].isObject())
  {
    QJsonObject obj = json["list"].toObject();
    read_ListElement(obj);
  }
  else if (json.contains("led") && json["led"].isObject())
  {
    QJsonObject obj = json["led"].toObject();
    read_LedElement(obj);
  }
  else if (json.contains("field") && json["field"].isObject())
  {
    QJsonObject obj = json["field"].toObject();
    read_FieldElement(obj);
  }
  else if (json.contains("action") && json["action"].isObject())
  {
    QJsonObject obj = json["action"].toObject();
    read_ActionElement(obj);
  }
  else if (json.contains("array") && json["array"].isObject())
  {
    QJsonObject obj = json["array"].toObject();
    read_ArrayElement(obj);
  }
  else if (json.contains("check") && json["check"].isObject())
  {
    QJsonObject obj = json["check"].toObject();
    read_CheckedElement(obj);
  }
  else if (json.contains("filter") && json["filter"].isObject())
  {
    QJsonObject obj = json["filter"].toObject();
    read_FilterElement(obj);
  }
}

void TControlWidgetGenerator::read_container(QJsonObject &json)
{
  cw->BeginContaner(jsonParser::direction(json), read_ElementName(json), read_ElementStretch(json));
  read_elements(json);
  cw->EndContainer();
}

bool TControlWidgetGenerator::read_tabs(QJsonObject &json)
{
  if (!(json.contains("tabs") && json["tabs"].isArray())) return false;
  cw->BeginTabWidget(read_ElementStretch(json));
  QJsonArray jsonTabs = json["tabs"].toArray();
  for (int i = 0; i < jsonTabs.size(); ++i)
  {
    QJsonObject obj = jsonTabs[i].toObject();
    read_tab_container(obj);
  }
  cw->EndTabWidget();
  return true;

}

void TControlWidgetGenerator::read_tab_container(QJsonObject &json)
{
  if (json.isEmpty())
    return;
  cw->BeginTabContainer(jsonParser::direction(json), read_ElementName(json));
  if (!read_elements(json))
    read_tabs(json);
  cw->EndContainer();
}

void TControlWidgetGenerator::read_ValueElementDefaults(QJsonObject &json)
{
  ElementDefaults.value.SendValueOnChanged = read_ValueElement_SendValueOnChanged(json);
}

void TControlWidgetGenerator::read_ValueElement(QJsonObject &json)
{
  Control::TValue * v = new Control::TValue(read_ElementId(json), read_ElementName(json), read_ValueElementPrec(json), read_ValueElement_SendValueOnChanged(json), jsonParser::direction(json));
  read_ElementBase(json, v);
  if (json.contains("range") && json["range"].isArray())
  {
    QJsonArray jsonRange = json["range"].toArray();
    v->setRange(jsonRange[0].toDouble(0), jsonRange[1].toDouble(99999), jsonRange[2].toDouble(1));
  }
  if (json.contains("value") && json["value"].isDouble())
    v->setValue(json["value"].toDouble());
  if (json.contains("suffix") && json["suffix"].isString())
    v->setSuffix(json["suffix"].toString());
  cw->appendToCurrentContainer(v, read_ElementStretch(json));
}

void TControlWidgetGenerator::read_ListElement(QJsonObject &json)
{
  Control::TList * l = new Control::TList(read_ElementId(json), read_ElementName(json), jsonParser::direction(json));

  read_ElementBase(json, l);
  if (json.contains("values") && json["values"].isArray())
  {
    QStringList vals;
    QJsonArray jsonVals = json["values"].toArray();
    for (int i = 0; i < jsonVals.size(); ++i)
      vals.append(jsonVals[i].toString(""));
    l->setValues(vals);
  }

  cw->appendToCurrentContainer(l, read_ElementStretch(json));
}

void TControlWidgetGenerator::read_LedElement(QJsonObject &json)
{
  Control::TLed * led = new Control::TLed(read_ElementId(json), read_ElementName(json), jsonParser::direction(json));

  read_ElementBase(json, led);

  cw->appendToCurrentContainer(led, read_ElementStretch(json));
}

void TControlWidgetGenerator::read_FieldElement(QJsonObject &json)
{
  Control::TField * field = new Control::TField(read_ElementId(json), read_ElementName(json), jsonParser::direction(json));

  read_ElementBase(json, field);
  if (json.contains("readOnly") && json["readOnly"].isBool())
  {
    bool readOnly = json["readOnly"].toBool();
    field->LineEdit()->setReadOnly(readOnly);
    field->setIsRuntimeFree(readOnly);
  }
  if (json.contains("text") && json["text"].isString())
  {
    field->LineEdit()->setText(json["text"].toString());
  }
  if (json.contains("maxlength") && json["maxlength"].isDouble())
  {
    field->LineEdit()->setMaxLength(json["maxlength"].toDouble());
  }

  cw->appendToCurrentContainer(field, read_ElementStretch(json));
}

void TControlWidgetGenerator::read_ActionElement(QJsonObject &json)
{
  Control::TAction * action = new Control::TAction(read_ElementId(json), read_ElementName(json), jsonParser::direction(json));

  read_ElementBase(json, action);

  cw->appendToCurrentContainer(action, read_ElementStretch(json));
}

void TControlWidgetGenerator::read_ArrayElement(QJsonObject &json)
{
  Control::TArray * el = new Control::TArray(read_ElementId(json), read_ElementName(json), jsonParser::direction(json));

  read_ElementBase(json, el);
  if (json.contains("values") && json["values"].isString())
    el->setValue(json["values"].toString());

  cw->appendToCurrentContainer(el, read_ElementStretch(json));
}

void TControlWidgetGenerator::read_CheckedElement(QJsonObject &json)
{
  Control::TCheckedElement * el = new Control::TCheckedElement(read_ElementId(json), read_ElementName(json), jsonParser::direction(json));

  read_ElementBase(json, el);

  cw->appendToCurrentContainer(el, read_ElementStretch(json));
}

void TControlWidgetGenerator::read_FilterElement(QJsonObject &json)
{
  Control::TFilter * el = new Control::TFilter(read_ElementId(json), read_ElementName(json), jsonParser::direction(json));

  read_ElementBase(json, el);
  if (json.contains("type") && json["type"].isString())
  {
    QString type = json["type"].toString();
    TFilterDesignWidget::TypeId typeId = TFilterDesignWidget::FIR;
    if (type == "fir")
      typeId = TFilterDesignWidget::FIR;
    el->FilterDesignWidget()->setType(typeId);
  }
  if (json.contains("firType") && json["firType"].isString())
  {
    QString firType = json["firType"].toString();
    TFilterDesignWidget::firTypeId firTypeId = TFilterDesignWidget::defaultFirTypeId;
    if (firType == "raw")
          firTypeId = TFilterDesignWidget::RawCoeffs;
    else if (firType == "window")
      firTypeId = TFilterDesignWidget::Window;
    else if (firType == "Parks-McClellan")
    {
      firTypeId = TFilterDesignWidget::ParksMcClellan;
      el->FilterDesignWidget()->setFirType(firTypeId);
      if (json.contains("bands") && json["bands"].isArray())
      {
        QJsonArray jsonBands = json["bands"].toArray();
        QVector<float> bands;
        bands.resize(jsonBands.size());
        for (int i = 0; i < jsonBands.size(); ++i)
          bands[i] = jsonBands[i].toDouble();
        el->FilterDesignWidget()->setBandsPMC(bands);
      }
      if (json.contains("resps") && json["resps"].isArray())
      {
        QJsonArray jsonResps = json["resps"].toArray();
        QVector<float> resps;
        resps.resize(jsonResps.size());
        for (int i = 0; i < jsonResps.size(); ++i)
          resps[i] = jsonResps[i].toDouble();
        el->FilterDesignWidget()->setDesiredRespPMC(resps);
      }
    }
  }
  if (json.contains("coeffs") && json["coeffs"].isString())
  {
    el->setValue(json["coeffs"].toString());
    switch(el->FilterDesignWidget()->type())
    {
    case TFilterDesignWidget::FIR:
      el->FilterDesignWidget()->setFirType(TFilterDesignWidget::RawCoeffs);
      break;
    default:
      break;
    }
  }
  if (json.contains("protectFs") && json["protectFs"].isBool())
    el->FilterDesignWidget()->setFsEnabled(!json["protectFs"].toBool());
  if (json.contains("Fs") && json["Fs"].isDouble())
    el->FilterDesignWidget()->setFs(json["Fs"].toDouble());
  if (json.contains("Gain") && json["Gain"].isDouble())
    el->FilterDesignWidget()->setGain(json["Gain"].toDouble());
  if (json.contains("Fc") && json["Fc"].isDouble())
    el->FilterDesignWidget()->setFc(json["Fc"].toDouble());  
  if (json.contains("As") && json["As"].isDouble())
    el->FilterDesignWidget()->setAs(json["As"].toDouble());
  if (json.contains("coeffsCount") && json["coeffsCount"].isDouble())
    el->FilterDesignWidget()->setCoeffsCount(json["coeffsCount"].toDouble());

  cw->appendToCurrentContainer(el, read_ElementStretch(json));
}

void TControlWidgetGenerator::read_ElementBase(QJsonObject &json, Control::TElement *el)
{
  if (json.contains("isStatusElement") && json["isStatusElement"].isBool() && json["isStatusElement"].toBool())
  {
    el->setIsManualEnabled(true);
    el->setEnabled(false);
  }
  else
  {
    if (json.contains("runtime") && json["runtime"].isBool())
      el->setIsRuntimeFriendly(json["runtime"].toBool());
    if (json.contains("manualEnabled") && json["manualEnabled"].isBool())
      el->setIsManualEnabled(json["manualEnabled"].toBool());
  }
}

int TControlWidgetGenerator::read_ElementId(QJsonObject &json)
{
  if (json.contains("id") && json["id"].isDouble())
    return json["id"].toInt();
  return -1;
}

QString TControlWidgetGenerator::read_ElementName(QJsonObject &json)
{
  if (json.contains("name") && json["name"].isString())
    return json["name"].toString();
  return QString();
}

int TControlWidgetGenerator::read_ElementStretch(QJsonObject &json)
{
  if (json.contains("stretch") && json["stretch"].isDouble())
    return json["stretch"].toInt(0);
  return 0;
}

bool TControlWidgetGenerator::read_ValueElement_SendValueOnChanged(QJsonObject &json)
{
  if (json.contains("SendValueOnChanged") && json["SendValueOnChanged"].isBool())
    return json["SendValueOnChanged"].toBool(false);
  return ElementDefaults.value.SendValueOnChanged;
}

int TControlWidgetGenerator::read_ValueElementPrec(QJsonObject &json)
{
  if (json.contains("type") && json["type"].isString())
  {
    QString typeStr = json["type"].toString();
    if ((typeStr == "i")||(typeStr == "int"))
      return 0;
  }
  QString precName;
  if (json.contains("prec") && json["prec"].isString())
    precName = "prec";
  else if (json.contains("precision") && json["precision"].isString())
    precName = "precision";
  else
    return 2;

  return json[precName].toInt();
}
