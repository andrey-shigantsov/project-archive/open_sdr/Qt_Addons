#ifndef TCONTROLWIDGET_H
#define TCONTROLWIDGET_H

#include <QWidget>
#include <QEvent>
#include <QBoxLayout>
#include <QMap>

#include <QLabel>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QComboBox>
#include <ledindicator.h>
#include <QLineEdit>
#include <QPushButton>
#include <QCheckBox>

#include "ArrayInputForm.h"
#include "TFilterDesignWidget.h"

namespace SDR
{

namespace Control
{
  class TElement : public QWidget
  {
    Q_OBJECT
  public:
    explicit TElement(const int id, const QString name, QBoxLayout::Direction dir, QWidget * parent = nullptr):
      QWidget(parent), Layout(dir,this)
    {
      Layout.setContentsMargins(0,0,0,0);
      setLayout(&Layout);

      this->id = id;
      this->name = name;

      flag_isManualEnabled = false;
      flag_isRuntimeFree = false;
      flag_IsRuntimeFriendly = false;
      flag_needRefreshValue = false;
    }
    virtual ~TElement(){}

    int Id(){return id;}

    void setName(const QString name){this->name = name; refreshName();}

    void setIsManualEnabled(bool flag){flag_isManualEnabled = flag;}
    bool IsManualEnabled(){return flag_isManualEnabled;}

    void setIsRuntimeFree(bool flag){flag_isRuntimeFree = flag;}
    bool IsRuntimeFree(){return flag_isRuntimeFree;}

    void setIsRuntimeFriendly(bool flag){flag_IsRuntimeFriendly = flag;}
    bool IsRuntimeFriendly(){return flag_IsRuntimeFriendly;}

    bool NeedRefreshValue(){return flag_needRefreshValue;}

    virtual void setStarted(bool flag)
    {
      if (!IsManualEnabled())
      {
        if (!IsRuntimeFree() && !IsRuntimeFriendly())
          setEnabled(!flag);
        else
          setEnabled(true);
      }
    }

  signals:
    void valueChanged(const int id, const QString &text);

  public slots:
    virtual void setEnabled(bool flag){QWidget::setEnabled(flag);}

    virtual QString Value() = 0;
    virtual void setValue(const QString &text) = 0;
    virtual void sendValue() = 0;

    virtual void readParams(const QString & paramsStr)
    {
      QStringList params = paramsStr.split(";");
      foreach (const QString &p, params)
      {
        if (p.isEmpty()) continue;
        QStringList x = p.split(":");
        SDR_ASSERT(x.count() == 2);
        setParam(x[0],x[1]);
      }
      if (flag_needRefreshValue)
        RefreshAndSendValue();
    }
    virtual bool setParam(const QString & name, const QString & value)
    {
      if (name == "name")
      {
        setName(value);
        return true;
      }
      else if (name == "enabled")
      {
        setEnabled(value == "true");
        return true;
      }
      else if (name == "container-enabled")
      {
        this->parentWidget()->setEnabled(value == "true");
        return true;
      }
      return false;
    }
    virtual void RefreshAndSendValue()
    {
      if (isEnabled())
      {
        refreshValue();
        sendValue();
        flag_needRefreshValue = false;
      }
    }

  protected:
    int id;
    QString name;

    bool flag_isManualEnabled, flag_isRuntimeFree, flag_IsRuntimeFriendly, flag_needRefreshValue;

    QBoxLayout Layout;

    virtual void refreshName() = 0;
    virtual void refreshValue() = 0;
  };

  class TLabledElement : public TElement
  {
    Q_OBJECT
  public:
    explicit TLabledElement(const int id, const QString name, QBoxLayout::Direction dir, QWidget * parent = nullptr):
      TElement(id,name,dir,parent)
    {
      Qt::Alignment alignment;
      switch(dir)
      {
      default:
      case QBoxLayout::LeftToRight:
        alignment = Qt::AlignLeft | Qt::AlignVCenter;
        break;
      case QBoxLayout::RightToLeft:
        alignment = Qt::AlignRight | Qt::AlignVCenter;
        break;
      case QBoxLayout::TopToBottom:
      case QBoxLayout::BottomToTop:
        alignment = Qt::AlignHCenter | Qt::AlignVCenter;
        break;
      }
      label.setAlignment(alignment);
      Layout.addWidget(&label);
      refreshName();
    }
  public slots:
    void setValue(const QString &text){setName(text);}
    void sendValue(){}
    QString Value(){return "";}
  protected:
    QLabel label;

    void refreshName()
    {
      label.setText(name);
      label.setVisible(!name.isEmpty());
    }
    void refreshValue(){}
  };

  class TCheckedElement : public TElement
  {
    Q_OBJECT
  public:
    explicit TCheckedElement(const int id, const QString name, QBoxLayout::Direction dir, QWidget * parent = nullptr):
      TElement(id,name,dir,parent)
    {
      refreshName();
      Layout.addWidget(&checkbox);

      connect(&checkbox, SIGNAL(toggled(bool)), this, SLOT(checked_handler(bool)));
    }

    void setChecked(bool flag){checkbox.setChecked(flag);}

  public slots:
    QString Value(){return get_check_state_string(checkbox.isChecked());}
    void setValue(const QString &text)
    {
      if (text == "checked")
        setChecked(true);
      else if (text == "unchecked")
        setChecked(false);
    }
    void sendValue(){checked_handler(checkbox.isChecked());}

  protected:
    QCheckBox checkbox;

    void refreshName(){checkbox.setText(name);}
    void refreshValue(){}
    QString get_check_state_string(bool flag){return flag?"checked":"unchecked";}

  protected slots:
    void checked_handler(bool flag){emit valueChanged(id, get_check_state_string(flag));}
  };

  class TAction : public TElement
  {
    Q_OBJECT
  public:
    explicit TAction(const int id, const QString name, QBoxLayout::Direction dir, QWidget * parent = nullptr):
      TElement(id,name,dir,parent)
    {
      setIsRuntimeFriendly(true);
      refreshName();
      Layout.addWidget(&button);
      connect(&button, SIGNAL(clicked()), this, SLOT(clicked_handler()));
    }

  protected:
    QPushButton button;

    void refreshName(){button.setText(name);}
    void refreshValue(){}

  public slots:
    QString Value(){return "";}
    void setValue(const QString &text){Q_UNUSED(text);}
    void sendValue(){}
  protected slots:
    void clicked_handler(){emit valueChanged(id, "");}
  };

  class TValue : public TLabledElement
  {
    Q_OBJECT
  public:
    explicit TValue(const int id, const QString name, const int prec, bool sendValueOnChanged, QBoxLayout::Direction dir, QWidget * parent = nullptr):
      TLabledElement(id, name, dir, parent)
    {
      fltBox = new QDoubleSpinBox();
      fltBox->setAlignment(Qt::AlignRight);
      Layout.addWidget(fltBox);

      if (sendValueOnChanged)
        connect(fltBox, SIGNAL(valueChanged(double)), this, SLOT(sendValue()));
      else
        connect(fltBox, SIGNAL(editingFinished()), this, SLOT(sendValue()));

      fltBox->setDecimals(prec);
    }
    ~TValue()
    {
    }

    void setSuffix(QString text)
    {
      fltBox->setSuffix(text);
    }

    void setPrecision(int prec)
    {
      fltBox->setDecimals(prec);
    }

    void setRange(double min, double max, double step)
    {
      fltBox->setRange(min,max);
      fltBox->setSingleStep(step);
    }

    void setValue(double value)
    {
      fltBox->setValue(value);
    }

  public slots:
    QString Value()
    {
      return QString::number(fltBox->value(),'f',log10(fltBox->maximum())+1+fltBox->decimals());
    }
    void setValue(const QString &text){setValue(text.toDouble());}
    void sendValue(){emit valueChanged(id, Value());}
    bool setParam(const QString & name, const QString & value)
    {
      if (name == "suffix")
      {
        flag_needRefreshValue = true;
        setSuffix(value);
        return true;
      }
      else if ((name == "prec")||(name == "precision"))
      {
        fltBox->setDecimals(value.toInt());
        return true;
      }
      else if (name == "min")
      {
        fltBox->setMinimum(value.toInt());
        return true;
      }
      else if (name == "max")
      {
        fltBox->setMaximum(value.toInt());
        return true;
      }
      else
        return TElement::setParam(name,value);
      return false;
    }
  protected:
    QDoubleSpinBox * fltBox;
  };

  class TList : public TLabledElement
  {
    Q_OBJECT
  public:
    explicit TList(const int id, const QString name, QBoxLayout::Direction dir, QWidget * parent = nullptr):
      TLabledElement(id, name, dir, parent)
    {
      box = new QComboBox(this);
      Layout.addWidget(box);

      connect(box, SIGNAL(activated(QString)), this, SLOT(value_handler(QString)));
    }
    void setValues(QStringList & list)
    {
      box->clear();
      box->addItems(list);
    }
  public slots:
    QString Value(){return box->currentText();}
    void setValue(const QString &text){box->setCurrentText(text);}
    void sendValue(){value_handler(box->currentText());}
  protected:
    QComboBox * box;
  protected slots:
    void value_handler(const QString &text){emit valueChanged(id, text);}
  };

  class TLed : public TLabledElement
  {
    Q_OBJECT
  public:
    explicit TLed(const int id, const QString name, QBoxLayout::Direction dir, QWidget * parent = nullptr):
      TLabledElement(id, name, dir, parent)
    {
      setIsRuntimeFree(true);

      led = new LedIndicator(this);
      led->setLedSize(10);
      led->setOffColor(QColor(Qt::darkGray));
      Layout.addWidget(led);

      connect(led, SIGNAL(clicked()), this, SLOT(sendValue()));
    }
    LedIndicator * Led(){return led;}
  public slots:
    QString Value(){return led->State() ? "on" : "off" ;}
    void setValue(const QString &text)
    {
      if(text == "on")
        led->setState(true);
      else if (text == "off")
        led->setState(false);
    }
    void sendValue(){emit valueChanged(id, led->State() ? "on" : "off");}
  protected:
    LedIndicator * led;
  };

  class TField : public TLabledElement
  {
    Q_OBJECT
  public:
    explicit TField(const int id, const QString name, QBoxLayout::Direction dir, QWidget * parent = nullptr):
      TLabledElement(id, name, dir, parent)
    {
      le = new QLineEdit(this);
      QSizePolicy sizePolicy = QSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
      sizePolicy.setHorizontalStretch(1);
      le->setSizePolicy(sizePolicy);
      Layout.addWidget(le);

      connect(le,SIGNAL(editingFinished()),this,SLOT(sendValue()));
    }
    QLineEdit * LineEdit(){return le;}
  public slots:
    QString Value(){return le->text();}
    void setValue(const QString &text)
    {
      le->setText(text);
    }
    void sendValue(){emit valueChanged(id, Value());}
    bool setParam(const QString & name, const QString & value)
    {
      if (name == "maxlength")
      {
        le->setMaxLength(value.toInt());
        return true;
      }
      else
        return TElement::setParam(name,value);
      return false;
    }
  protected:
    QLineEdit * le;
  };
  class TArray : public TLabledElement
  {
    Q_OBJECT
  public:
    explicit TArray(const int id, const QString name, QBoxLayout::Direction dir, QWidget * parent = nullptr):
      TLabledElement(id, name, dir, parent),
      editButton("edit")
    {
      Layout.addWidget(&editButton);

      connect(&editButton, SIGNAL(clicked()), this, SLOT(on_editButton_clicked()));
    }

  public slots:
    QString Value(){return valuesString;}
    void setValue(const QString &text)
    {
      valuesString = text;
    }
    void sendValue()
    {
      emit valueChanged(id,valuesString.simplified());
    }

  protected:
    QString valuesString;
    QVector<Sample_t> coeffs;
    QPushButton editButton;

  protected slots:
    void on_editButton_clicked()
    {
      if (!ArrayInputForm::editSamples(valuesString, coeffs, name)) return;
      sendValue();
    }
  };
  class TFilter : public TLabledElement
  {
    Q_OBJECT
  public:
    explicit TFilter(const int id, const QString name, QBoxLayout::Direction dir, QWidget * parent = nullptr):
      TLabledElement(id, name, dir, parent),
      designButton("design")
    {
      flag_needRefreshValue = true;

      Widget.setWindowFlags(Qt::SubWindow);
      Widget.setWindowTitle(name + " (design)");
      Widget.resize(0,0);

      Layout.addWidget(&designButton);

      connect(&designButton, SIGNAL(clicked()), this, SLOT(showWidget()));
      connect(&Widget,SIGNAL(coeffsChanged()),this,SLOT(sendValue()));
    }

    TFilterDesignWidget * FilterDesignWidget(){return &Widget;}

  public slots:
    QString Value(){return "";}
    void setValue(const QString &text){Widget.setCoeffs(text);}
    void sendValue(){emit valueChanged(id,Widget.coeffsString());}
    void setEnabled(bool flag)
    {
      Widget.setEditable(flag);
      TElement::setEnabled(flag);
    }
    bool setParam(const QString & name, const QString & value)
    {
      if (name == "Fs")
      {
        flag_needRefreshValue = true;
        Widget.setFs(value.toDouble());
        return true;
      }
      else if (name == "Fc")
      {
        flag_needRefreshValue = true;
        Widget.setFc(value.toDouble());
        return true;
      }
      else
        return TElement::setParam(name,value);
      return false;
    }

  protected:
    QPushButton designButton;
    TFilterDesignWidget Widget;

    void refreshValue(){Widget.raw_design();}

  protected slots:
    void showWidget(){Widget.show();}
  };
}

class TControlWidget : public QWidget
{
  Q_OBJECT
public:
  explicit TControlWidget(QWidget *parent = nullptr);
  explicit TControlWidget(QBoxLayout::Direction dir = QBoxLayout::TopToBottom, QWidget *parent = nullptr);

  void setDirection(QBoxLayout::Direction dir);

  void BeginTabWidget(int stretch = 0);
  void EndTabWidget();

  void BeginContaner(QBoxLayout::Direction dir = QBoxLayout::LeftToRight, QString name = QString(), int stretch = 0);
  void BeginTabContainer(QBoxLayout::Direction dir = QBoxLayout::LeftToRight, QString name = QString());
  void EndContainer();

  void appendToCurrentContainer(Control::TElement * element, int stretch = 0);
  void appendStretchToCurrentContainer(int stretch = 0);

  void setIsStarted(bool flag);

signals:
  void valueChanged(const int id, const QString &text);

public slots:
  QString Value(const int id);
  void setValue(const int id, const QString &text);

  void setParams(const int id, const QString &text);

  void sendNeededValues();
  void sendAllValues();

protected:
  QBoxLayout Layout;
  QList<QBoxLayout*> LayoutStack;
  QList<QTabWidget*> TabWidgetStack;
  QMap<int,Control::TElement*> Elements;

  QBoxLayout *createNewLayout(QBoxLayout::Direction dir);
  void BeginContaner(bool isTab, QBoxLayout::Direction dir = QBoxLayout::LeftToRight, QString name = QString(), int stretch = 0);

protected slots:
  void value_handler(const int id, const QString &value){emit valueChanged(id, value);}
};

}

#endif // TCONTROLWIDGET_H
