#ifndef TCONTROLWIDGETGENERATOR_H
#define TCONTROLWIDGETGENERATOR_H

#include "TControlWidget.h"

#include <QMap>
#include <QJsonObject>

namespace SDR
{
namespace Control
{
struct TValueElementDefaults
{
public:

  TValueElementDefaults()
  {
    SendValueOnChanged = false;
  }

  bool SendValueOnChanged;
};

}
class TControlWidgetGenerator : public QObject
{
  Q_OBJECT
public:
  explicit TControlWidgetGenerator(TControlWidget * cw, QObject *parent = nullptr);

  static TControlWidget * create(const char * jsonFormat, QWidget * parent);
  static bool read(const char * jsonFormat, TControlWidget * cw);
  static bool read(QJsonObject & json, TControlWidget * cw);

signals:

public slots:

protected: 
  TControlWidget * cw;
  struct
  {
    Control::TValueElementDefaults value;
  } ElementDefaults;

  bool read_root_contant(QJsonObject & json);

  bool read_defaults(QJsonObject & json);
  bool read_elements(QJsonObject & json);
  void read_default(QJsonObject & json);
  void read_element(QJsonObject & json);

  void read_container(QJsonObject & json);

  bool read_tabs(QJsonObject & json);
  void read_tab_container(QJsonObject & json);

  void read_ValueElementDefaults(QJsonObject & json);
  void read_ValueElement(QJsonObject & json);
  void read_ListElement(QJsonObject & json);
  void read_LedElement(QJsonObject & json);
  void read_FieldElement(QJsonObject & json);
  void read_ActionElement(QJsonObject & json);
  void read_ArrayElement(QJsonObject & json);
  void read_CheckedElement(QJsonObject & json);
  void read_FilterElement(QJsonObject & json);

  void read_ElementBase(QJsonObject & json, Control::TElement * el);
  int read_ElementId(QJsonObject &json);
  QString read_ElementName(QJsonObject & json);
  int read_ElementStretch(QJsonObject & json);

  bool read_ValueElement_SendValueOnChanged(QJsonObject & json);
  int read_ValueElementPrec(QJsonObject & json);
};

}

#endif // TCONTROLWIDGETGENERATOR_H
