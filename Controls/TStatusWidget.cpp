#include "TStatusWidget.h"

using namespace SDR;

TStatusWidget::TStatusWidget(QWidget *parent) : TStatusWidget(QBoxLayout::TopToBottom, parent){}

TStatusWidget::TStatusWidget(QBoxLayout::Direction dir, QWidget *parent) :
  QWidget(parent), Layout(dir,this)
{
  Layout.setContentsMargins(0,0,0,0);

  curLineLayout = 0;
}

void TStatusWidget::setDirection(QBoxLayout::Direction dir)
{
  Layout.setDirection(dir);
}

void TStatusWidget::NewLine(QBoxLayout::Direction dir)
{
  createNewLineLayout(dir);
}

void TStatusWidget::appendToCurrentLine(Status::TElement *element)
{
  if (!curLineLayout)
    NewLine();
  curLineLayout->addWidget(element);

  Elements[element->Id()] = element;
}

void TStatusWidget::appendStretchToCurrentLine(int stretch)
{
  if (!curLineLayout)
    NewLine();

  curLineLayout->addStretch(stretch);
}

void TStatusWidget::setIsStarted(bool flag)
{
  foreach (Status::TElement * el, Elements)
    el->setEnabled(!flag);
}

void TStatusWidget::createNewLineLayout(QBoxLayout::Direction dir)
{
  curLineLayout = new QBoxLayout(dir);
  curLineLayout->setContentsMargins(0,0,0,0);
  Layout.addLayout(curLineLayout);
}

QString TStatusWidget::Value(const int id)
{
  return Elements[id]->Value();
}

void TStatusWidget::setValue(const int id, const QString &text)
{
  if (!Elements.contains(id))
    return;
  Elements[id]->setValue(text);
}

void TStatusWidget::setParams(const int id, const QString &text)
{
  if (!Elements.contains(id))
    return;
  Elements[id]->readParams(text);
}
