#ifndef LEDINDICATOR_H
#define LEDINDICATOR_H

#include <QWidget>

class LedIndicator: public QWidget {
    Q_OBJECT
  public:
    LedIndicator(QWidget *parent = 0);
    bool State(){return lit;}
    void setState(bool state);
    void toggle();
    void setOnColor(QColor onColor);
    void setOffColor(QColor offColor);
    void setOnPattern(Qt::BrushStyle onPattern);
    void setOffPattern(Qt::BrushStyle offPattern);
    void setLedSize(int size);
    void setMargins(int size);

  signals:
    void clicked();

  public slots:
    void switchLedIndicator();
  protected:
    void paintEvent(QPaintEvent *);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
  private:
    bool lit, MouseLeftButtonClicked;
    QColor ledOnColor;
    QColor ledOffColor;
    Qt::BrushStyle ledOnPattern;
    Qt::BrushStyle ledOffPattern;
    int ledSize, ledPosAllowance, marginSize, yOffsetSize;
    QRect ledDrawRect;
    void refreshSizesAndPos();
    bool localPosInLed(QPoint pos);
};

#endif // LEDINDICATOR_H

