#include "ledindicator.h"
#include <QPainter>
#include <QMouseEvent>

#include <QDebug>

LedIndicator::LedIndicator(QWidget *parent) :
    QWidget(parent)
{
  MouseLeftButtonClicked = false;
  lit = false;
  ledOnColor=Qt::green;
  ledOffColor=Qt::red;
  ledOnPattern = Qt::SolidPattern;
  ledOffPattern = Qt::SolidPattern;
  ledSize=10;
  ledPosAllowance = 4;
  marginSize = 4;
  yOffsetSize = 4;
  refreshSizesAndPos();
}

void LedIndicator::paintEvent(QPaintEvent *) {
  QPainter p(this);
  lit ?  p.setBrush(QBrush(ledOnColor, ledOnPattern)) : p.setBrush(QBrush(ledOffColor, ledOffPattern));
  p.drawEllipse(ledDrawRect);
}

void LedIndicator::mousePressEvent(QMouseEvent *event)
{
  MouseLeftButtonClicked = (event->button() == Qt::LeftButton) && localPosInLed(event->pos());
}

void LedIndicator::mouseReleaseEvent(QMouseEvent *event)
{
  if ((event->button() == Qt::LeftButton) && MouseLeftButtonClicked && localPosInLed(event->pos()))
  {
    MouseLeftButtonClicked = false;
    emit clicked();
  }
}

void LedIndicator::refreshSizesAndPos()
{
  ledDrawRect.setX(marginSize/2);
  ledDrawRect.setY(marginSize/2);
  ledDrawRect.setWidth(ledSize);
  ledDrawRect.setHeight(ledSize);

  int size = ledSize + marginSize;
  setFixedSize(size, size+yOffsetSize);
}

bool LedIndicator::localPosInLed(QPoint pos)
{
  QPoint ledCenter = ledDrawRect.center();
  float dx = pos.x() - ledCenter.x();
  float dy = pos.y() - ledCenter.y();
  float r = ledDrawRect.height()/2 + ledPosAllowance;
  return dx*dx + dy*dy <= r*r;
}

void LedIndicator::switchLedIndicator() {
  lit = ! lit;
  repaint();
}
void LedIndicator::setState(bool state)
{
    lit = state;
    repaint();
}
void LedIndicator::toggle()
{
  lit = ! lit;
  repaint();
}

void LedIndicator::setOnColor(QColor onColor)
{
  ledOnColor=onColor;
  repaint();
}
void LedIndicator::setOffColor(QColor offColor)
{
  ledOffColor=offColor;
  repaint();
}
void LedIndicator::setOnPattern(Qt::BrushStyle onPattern)
{
  ledOnPattern=onPattern;
  repaint();
}
void LedIndicator::setOffPattern(Qt::BrushStyle offPattern)
{
  ledOffPattern=offPattern;
  repaint();
}
void LedIndicator::setLedSize(int size)
{
  ledSize=size;
  refreshSizesAndPos();
  repaint();
}

void LedIndicator::setMargins(int size)
{
  marginSize = size;
  refreshSizesAndPos();
  repaint();
}
