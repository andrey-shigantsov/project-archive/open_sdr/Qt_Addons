#include <QDebug>
#include "TAudioInput.h"

#include <qendian.h>

using namespace SDR;

TAudioInput::TAudioInput(const QAudioFormat & format, QObject *parent) :
  QObject(parent)
{
  input = 0;
  setFormat(format);
}

TAudioInput::~TAudioInput()
{
  delete_input();
}

void TAudioInput::setFs(Frequency_t Fs)
{
  if (!input)
  {
    qWarning().noquote() << "SDR::TAudioInput: set Fs failure: input is nullptr";
    return;
  }

  QAudioFormat format = input->format();
  format.setSampleRate(Fs);
  setFormat(format);
}

void TAudioInput::setSink(void *Master, samples_sink_t handler)
{
  QAudioFormat format = input->format();
  format.setChannelCount(1);
  setFormat(format);
  inputDevice.setSink(Master, handler);
}

void TAudioInput::setSink(void *Master, samples_sink_iq_t handler)
{
  QAudioFormat format = input->format();
  format.setChannelCount(2);
  setFormat(format);
  inputDevice.setSink(Master, handler);
}

void TAudioInput::start()
{
  if (!input)
  {
    qWarning().noquote() << "SDR::TAudioInput: start failure: input is nullptr";
    return;
  }

  inputDevice.start();
  if (!inputDevice.isOpen())
    return;
  input->start(&inputDevice);
  if (input->error() != QAudio::NoError)
    qWarning().noquote() << "SDR::TAudioInput: start failure:" << input->error();
}

void TAudioInput::stop()
{
  input->stop();
  inputDevice.stop();
}

void TAudioInput::delete_input()
{
  if (input)
  {
    disconnect(input, SIGNAL(stateChanged(QAudio::State)), this, SLOT(inputStateHandler(QAudio::State)));
    if (input->state() != QAudio::StoppedState)
      stop();
    delete input;
  }
}

void TAudioInput::inputStateHandler(QAudio::State state)
{

}

void TAudioInput::setFormat(QAudioFormat format)
{
  delete_input();

  QAudioDeviceInfo inputInfo = QAudioDeviceInfo::defaultInputDevice();
  if (!inputInfo.isFormatSupported(format))
  {
    format = inputInfo.nearestFormat(format);
    qWarning().noquote() << "SDR::TAudioInput: set format failure: trying to use nearest: " << format;
  }

  inputDevice.setFormat(format);
  input = new QAudioInput(inputInfo,format);
  connect(input, SIGNAL(stateChanged(QAudio::State)), this, SLOT(inputStateHandler(QAudio::State)));
}

void TAudioInputDevice::start()
{
  if (!open(QIODevice::WriteOnly))
    qWarning().noquote() << "SDR::TAudioInputDevice: open failure:" << this->errorString();
}

qint64 TAudioInputDevice::readData(char *data, qint64 maxlen)
{
  Q_UNUSED(data)
  Q_UNUSED(maxlen)
  return 0;
}

qint64 TAudioInputDevice::writeData(const char *data, qint64 len)
{
  if (!((!isComplex && handler)||(isComplex && handler_iq)))
    return 0;

  int chsCount = m_format.channelCount();
  Q_ASSERT((isComplex && (chsCount == 2))||(!isComplex && (chsCount == 1)));

  int sampleSize = m_format.sampleSize();
  Q_ASSERT(sampleSize % 8 == 0);

  const int channelBytes = sampleSize / 8;
  const int sampleBytes = chsCount * channelBytes;
  Q_ASSERT(len % sampleBytes == 0);
  const int numSamples = len / sampleBytes;

#define NEED_IMPLEMENTATION Q_ASSERT(0)
  if (m_format.sampleSize() == 8 && m_format.sampleType() == QAudioFormat::UnSignedInt)
  {
    const quint8 * ptr = reinterpret_cast<const quint8*>(data);
    NEED_IMPLEMENTATION;
  }
  else if (m_format.sampleSize() == 8 && m_format.sampleType() == QAudioFormat::SignedInt)
  {
    const qint8 * ptr = reinterpret_cast<const qint8*>(data);
    NEED_IMPLEMENTATION;
  }
  else if (m_format.sampleSize() == 16 && m_format.sampleType() == QAudioFormat::UnSignedInt)
  {
    quint16 value;
    if (m_format.byteOrder() == QAudioFormat::LittleEndian)
      value = qFromLittleEndian<quint16>(data[0]);
    else
      value = qFromBigEndian<quint16>(data[0]);
    NEED_IMPLEMENTATION;
  }
  else if (m_format.sampleSize() == 16 && m_format.sampleType() == QAudioFormat::SignedInt)
  {
    QVector<Sample_t> Samples;
    Samples.resize(chsCount*numSamples);
    for (int i = 0, j = 0; i < len; i+=channelBytes,++j)
    {
      qint16 value;
      if (m_format.byteOrder() == QAudioFormat::LittleEndian)
        value = qFromLittleEndian<qint16>(data[i]);
      else
        value = qFromBigEndian<qint16>(data[i]);
      Samples[j] = ((Sample_t)value)/INT16_MAX;
    }
    if (isComplex)
      handler_iq(Master, reinterpret_cast<iqSample_t*>(Samples.data()), numSamples);
    else
      handler(Master, Samples.data(), numSamples);
  }
  else if (m_format.sampleSize() == 32 && m_format.sampleType() == QAudioFormat::UnSignedInt)
  {
    quint32 value;
    if (m_format.byteOrder() == QAudioFormat::LittleEndian)
      value = qFromLittleEndian<quint32>(data[0]);
    else
      value = qFromBigEndian<quint32>(data[0]);
    NEED_IMPLEMENTATION;
  }
  else if (m_format.sampleSize() == 32 && m_format.sampleType() == QAudioFormat::SignedInt)
  {
    qint32 value;
    if (m_format.byteOrder() == QAudioFormat::LittleEndian)
      value = qAbs(qFromLittleEndian<qint32>(data[0]));
    else
      value = qAbs(qFromBigEndian<qint32>(data[0]));
    NEED_IMPLEMENTATION;
  }
  else if (m_format.sampleSize() == 32 && m_format.sampleType() == QAudioFormat::Float)
  {
    Q_ASSERT(sizeof(Sample_t) == 32);
    if (isComplex)
    {
      iqSample_t * ptr = reinterpret_cast<iqSample_t *>((char*)data);
      handler_iq(Master, ptr, numSamples);
    }
    else
    {
      Sample_t * ptr = reinterpret_cast<Sample_t *>((char*)data);
      handler(Master, ptr, numSamples);
    }
  }
  else
    Q_ASSERT(0);
  return len;
}
